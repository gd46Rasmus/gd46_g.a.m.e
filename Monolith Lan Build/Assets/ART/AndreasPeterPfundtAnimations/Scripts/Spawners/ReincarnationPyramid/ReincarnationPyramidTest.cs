﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReincarnationPyramidTest : MonoBehaviour
{
	Animator aReincarnationPyramid;

	void Awake ()
	{
		aReincarnationPyramid = GetComponent <Animator> ();
	}

	void Update ()
	{
		States (); // QUE ANIMATIONS.
	}

	// QUE ANIMATIONS.
	void States ()
	{
		if (aReincarnationPyramid.GetBool ("bIsOpening") == false &&
			Input.GetKeyDown (KeyCode.Space)) // PRESS SPACE TO OPEN.
		{
			aReincarnationPyramid.SetBool ("bIsOpening", true);
		}
		else if (aReincarnationPyramid.GetBool ("bIsOpening") == true &&
			Input.GetKeyDown (KeyCode.Space)) // // PRESS SPACE TO CLOSE.
		{
			aReincarnationPyramid.SetBool ("bIsOpening", false);
		}
	}
}