﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngagerTest : MonoBehaviour
{
	public GameObject gOLandMine;

	private Animator aEngager;

	void Awake ()
	{
		aEngager = GetComponent <Animator> ();
	}

	void Update ()
	{
		Controls (); // QUE ANIMATIONS.
	}

	// QUE ANIMATIONS.
	void Controls ()
	{
		if (Input.GetKey (KeyCode.W)) // PRESS W TO RUN.
		{
			aEngager.SetBool ("bIsRunning", true);
		}
		else // RELEASE W TO RUN.
		{
			aEngager.SetBool ("bIsRunning", false);
		}

		if (Input.GetKey (KeyCode.A)) // PRESS A TO THROW.
		{
			aEngager.SetTrigger ("tIsThrowing");

			gOLandMine.SetActive (false);
		}
		else if (Input.GetKeyDown (KeyCode.D)) // PRESS D TO DETONATE.
		{
			aEngager.SetTrigger ("tIsDetonating");

			gOLandMine.SetActive (true);
		}
	}
}