﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowWalkerTest : MonoBehaviour
{
	private Animator aShadowWalker;

	void Awake ()
	{
		aShadowWalker = GetComponent <Animator> ();
	}

	void Update ()
	{
		Controls (); // QUE ANIMATIONS.
	}

	// QUE ANIMATIONS.
	void Controls ()
	{
		if (Input.GetKey (KeyCode.W)) // PRESS W TO RUN.
		{
			aShadowWalker.SetBool ("bIsRunning", true);
		}
		else // RELEASE W TO CANCEL RUN.
		{
			aShadowWalker.SetBool ("bIsRunning", false);
		}

		if (Input.GetKeyDown (KeyCode.A)) // PRESS A TO SWING.
		{
			aShadowWalker.SetTrigger ("tIsSwinging");
		}
		else if (Input.GetKeyDown (KeyCode.D)) // PRESS S TO THRUST.
		{
			aShadowWalker.SetTrigger ("tIsThrusting");
		}
	}
}