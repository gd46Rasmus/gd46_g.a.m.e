﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharpSightTest : MonoBehaviour
{
	private Animator aSharpSight;

	void Awake ()
	{
		aSharpSight = GetComponent <Animator> ();
	}

	void Update ()
	{
		Controls (); // QUE ANIMATIONS.
	}

	// QUE ANIMATIONS.
	void Controls ()
	{
		if (Input.GetKey (KeyCode.W)) // PRESS W TO RUN.
		{
			aSharpSight.SetBool ("bIsRunning", true);
		}
		else // RELEASE W TO CANCEL RUN.
		{
			aSharpSight.SetBool ("bIsRunning", false);
		}

		if (aSharpSight.GetBool ("bIsAiming") == false &&
			Input.GetKeyDown (KeyCode.A)) // PRESS A TO SHOOT.
		{
			aSharpSight.SetTrigger ("tIsShooting");
		}
		else if (aSharpSight.GetBool ("bIsAiming") == false &&
			Input.GetKeyDown (KeyCode.D)) // PRESS D TO READY AND AIM.
		{
			aSharpSight.SetBool ("bIsAiming", true);
			aSharpSight.SetTrigger ("tIsReadying");
		}
		else if (aSharpSight.GetBool ("bIsAiming") == true &&
			Input.GetKeyDown (KeyCode.D)) // PRESS D TO CANCEL AIM.
		{
			aSharpSight.SetBool ("bIsAiming", false);
		}

		if (aSharpSight.GetBool ("bIsAiming") == true &&
			Input.GetKeyDown (KeyCode.A)) // PRESS A TO SNIPE.
		{
			aSharpSight.SetBool ("bIsAiming", false);
			aSharpSight.SetTrigger ("tIsSniping");
		}
	}
}