﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using XInputDotNetPure;

public class Range : Character {
    
    public float reloadSpeed = .5f;
    public float snipeReloadSpeed = 2;
    public Projectile NormalProjectile;
    public Projectile SniperProjectile;

    public LineRenderer sniperLine;

    [SyncVar(hook = "SetSnipe")]private bool inSnipeMode = false;

    private float normalFovAngle;
    private float normalFovDist;

    public PolygonCollider2D sniperTrigger;

    public float zCord = -3;

    public float snipeRange = 900;

    public float snipeAngle = 45;
    public float snipeDist = 600;
    public float snipeZoom = 100;
    public float snipeMoveSpeed = .2f;
    private float normalSpeed;

    public float snipeCameraAhead = 40;

    private float normalAhead;
    
    public float shotGunAngle = 33;
    public int shotGunShells = 10;

    public float shotGunCharge = .2f;
    float chargeTime = 0;

    private bool playerReleased = true;

    public LayerMask shootLayers;

    float releaseTime = 0;
    bool specialReady = true;

    protected override void Start()
    {
        base.Start();
        normalAhead = cameraAheadDist;
        normalFovAngle = fieldOfView.viewAngle;
        normalFovDist = fieldOfView.viewRadius;
        normalSpeed = speed;
    }
    public override void Attack()
    {
        if (specialReady && inSnipeMode)
        { 
            ShootSniper();
            StartCoroutine(SpecialCooldown(snipeReloadSpeed));
            
        }
    }

    [Command]
    void Cmd_ForceLight()
    {
        fov.enabled = true;

        fieldOfView.useCircleOnly = !fov.enabled;
        fovColor.enabled = fov.enabled;
        
        Rpc_ForceLight();
    }

    [ClientRpc]
    void Rpc_ForceLight()
    {
        fov.enabled = true;

        fieldOfView.useCircleOnly = !fov.enabled;
        fovColor.enabled = fov.enabled;
        minimapPointLocal.enabled = fov.enabled;

        if(hasAuthority)
        {
            minimapPointLocal.enabled = fov.enabled;
        }
    }

    protected override void Update()
    {
        base.Update();

        if(inSnipeMode && !fov.enabled)
        {
            Cmd_ForceLight();
        }

        if (!hasAuthority) return;
        if (!inSnipeMode && owner.Attacking && canAttack && playerReleased)
        {
            Cmd_ShotGunAnim(true);
            if ((chargeTime += Time.deltaTime) >= shotGunCharge)
            {

                
                    
                ShootShotgun();
                playerReleased = false;
                GamePad.SetVibration(PlayerIndex.One, 2, 2);
                StartCoroutine(doHaptic(.2f));
                StartCoroutine(AttackCooldown(reloadSpeed));
            }
            else
            {
                GamePad.SetVibration(PlayerIndex.One, (chargeTime / shotGunCharge) / 2, (chargeTime / shotGunCharge) / 2);
            }
            releaseTime = 0;
            
        }
        else if (!owner.Attacking || !canAttack)
        {

            Cmd_ShotGunAnim(false);
            if (!owner.Attacking) playerReleased = true;
            chargeTime = 0;
        }
        else
        {
            Cmd_ShotGunAnim(false);
        }

        if((releaseTime += Time.deltaTime) >= .2f && canAttack)
        {
            GamePad.SetVibration(PlayerIndex.One, (chargeTime / shotGunCharge), (chargeTime / shotGunCharge));
        }
        
    }

    public override void Rotate(float x, float y)
    {
        if(chargeTime == 0)
        {
            base.Rotate(x, y);
        }
    }

    private void ShootShotgun()
    {
        Cmd_ShotGun(sprite.transform.eulerAngles.z);
    }

    private void ShootSniper()
    {
        RaycastHit2D h = Physics2D.Raycast(projectileSpawner.transform.position, sprite.transform.up, snipeRange, shootLayers);
        Character c = null;
        
        ContactFilter2D cf = new ContactFilter2D();
        Collider2D[] colid = new Collider2D[100];
        
        sniperTrigger.OverlapCollider(cf, colid);

        foreach (Collider2D cd in colid)
        {
            if(cd != null)
            {
                if((c = cd.gameObject.GetComponentInParent<Character>()) && c != this)
                {
                    break;
                }
            }

        }
        


        if (c != null && c != this)
        {
            Cmd_Snipe(c.gameObject);
        }
        else
        {
            if (h.transform != null)
            {
                
                Cmd_SnipeNoTarget(new Vector3(h.point.x, h.point.y, 0));
            }
            else
            {
                Vector3 pos = transform.up * (snipeRange * .5f) + transform.position;
                Cmd_SnipeNoTarget(pos);
            }
        }

        GamePad.SetVibration(owner.GetPlayerIndex(), 1, 1);
        StartCoroutine(doHaptic(.1f));

    }

    private void Shoot_Local(float hapticDuration, float reloadTime)
    {
        GamePad.SetVibration(owner.GetPlayerIndex(), 1, 1);
        StartCoroutine(doHaptic(hapticDuration));
    }

    [Command]
    private void Cmd_ShotGun(float zRot)
    {

        zRot += 90;

        for(int i = 0; i < shotGunShells; i++)
        {
            

            float rot = zRot - (shotGunAngle / 2);

            rot += (shotGunAngle / shotGunShells) * i;
            

            Projectile p = Instantiate(NormalProjectile, projectileSpawner.transform.position, Quaternion.Euler(0, 0, rot));

            p.gunner = gameObject;

            NetworkServer.Spawn(p.gameObject);
        }

        

        Sounds.clip = SoundLibrary.shotGunSouds;
        Sounds.Play();

        Rpc_ShotGun();

    }

    [ClientRpc]
    void Rpc_ShotGun()
    {
        Sounds.clip = SoundLibrary.shotGunSouds;
        Sounds.Play();
    }
     
    [Command]
    void Cmd_ShotGunAnim(bool val)
    {
        anim.SetBool("bIsCharging", val);
        Rpc_ShotgunAnim(val);
    }

    [ClientRpc]
    void Rpc_ShotgunAnim(bool val)
    {
        anim.SetBool("bIsCharging", val);
    }
   

    [Command]
    private void Cmd_Snipe(GameObject target)
    {
        anim.SetTrigger("tIsSniping");
        Sounds.clip = SoundLibrary.Sniper;
        Sounds.Play();
        inSnipeMode = false;
        Rpc_Snipe(target);
    }

    [Command]
    private void Cmd_SnipeNoTarget(Vector3 hit)
    {

        anim.SetTrigger("tIsSniping");
        Sounds.clip = SoundLibrary.Sniper;
        Sounds.Play();
        inSnipeMode = false;
        Rpc_SnipeNoTarget(hit);
    }

    [ClientRpc]
    private void Rpc_Snipe(GameObject target)
    {

        anim.SetTrigger("tIsSniping");
        Sounds.clip = SoundLibrary.Sniper;
        Sounds.Play();
        LineRenderer line = Instantiate(sniperLine);


        Vector3[] pos = { transform.position, target.transform.position };
        pos[0].z = zCord;
        pos[1].z = zCord;
        line.SetPositions(pos);

        Character t = target.GetComponent<Character>();

        if(t != null)
        {
            t.TakeDamage(1, gameObject);
        }
    }

    [ClientRpc]
    private void Rpc_SnipeNoTarget(Vector3 hit)
    {

        anim.SetTrigger("tIsSniping");
        Sounds.clip = SoundLibrary.Sniper;
        Sounds.Play();
        LineRenderer line = Instantiate(sniperLine);
        

        Vector3[] pos = { transform.position,  hit};
        pos[0].z = zCord;
        pos[1].z = zCord;
        line.SetPositions(pos);
    }

    public override void SpecialMove()
    {
        if (specialReady)
        {
            Cmd_Scope();
        }
    }

    [Command]
    void Cmd_Scope()
    {
        inSnipeMode = !inSnipeMode;
    }

    void SetSnipe(bool updatedBool)
    {
        if(updatedBool)
        {
            anim.SetTrigger("tIsReadying");
            inSnipeMode = true;
            fieldOfView.viewAngle = snipeAngle;
            fieldOfView.viewRadius = normalFovDist + snipeDist;
            speed = snipeMoveSpeed;
            cameraAheadDist = normalAhead + snipeCameraAhead;
            owner.playerCamera.GetComponent<Camera>().orthographicSize = owner.playerCamera.GetComponent<CameraController>().cameraZoom + snipeZoom;
        }
        else
        {

            anim.SetTrigger("tResetAim");
            inSnipeMode = false;
            fieldOfView.viewAngle = normalFovAngle;
            fieldOfView.viewRadius = normalFovDist;
            owner.playerCamera.GetComponent<Camera>().orthographicSize = owner.playerCamera.GetComponent<CameraController>().cameraZoom;
            speed = normalSpeed;
            cameraAheadDist = normalAhead;
        }
    }


    IEnumerator SpecialCooldown(float cd)
    {
        specialReady = false;
        yield return new WaitForSeconds(cd);
        specialReady = true;
    }
}
