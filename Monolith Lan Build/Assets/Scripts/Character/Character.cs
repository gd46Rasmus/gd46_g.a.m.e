﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using XInputDotNetPure;

public class Character : NetworkBehaviour {

    public SpriteRenderer blood;

    public Color minimapColor = new Color(1, 1, 1);
    bool isDead = false;
    float rayPadding;
    public float dashMiss = .3f;
    protected bool isDashing;
    public float dashSpeed = 5;
    float normalDashSpeed;
    public float DashMaxDistance = 5;
    public LayerMask dashMask;
    Vector3 dashTarget;
    bool dashCd;

    public SpriteRenderer minimapPoint;
    public SpriteRenderer minimapPointLocal;

    public Animator anim;

    public float speed;

    public float cameraAheadDist = 28;

    //References
    public MeshRenderer fov;
    public MeshRenderer fovColor;
    public MeshRenderer area;


    public Sword meleeObject;
    public GameObject projectileSpawner;
    public PlayerController owner;
    protected Rigidbody2D rigid;
    public GameObject sprite;
    protected FieldOfView fieldOfView;

    public GameObject cameraPoint;

    CircleCollider2D debugCol;

    private List<Character> currentCharacters = new List<Character>();

    //to be removed later

    private GameMode gm;


    [SyncVar] protected bool canAttack = true;

    //sprint variables

    AudioClip[] grunts;

    public AudioSource Sounds;
    public AudioSource flashLightSounds;

    public AudioSource diePlayer;

    private float respawnInvinciblity = 3;
    [SyncVar] private bool invurnable = true;
    //stuffs
    public Color charColor;
    protected int playerIndex;



    [Command]
    void Cmd_Announce(GameObject newchar)
    {
        Rpc_AddChar(newchar);
    }

    [ClientRpc]
    void Rpc_AddChar(GameObject newchar)
    {
        currentCharacters.Add(newchar.GetComponent<Character>());
    }

    // Use this for initialization
    protected virtual void Start()
    {
        normalDashSpeed = dashSpeed;
        anim = GetComponentInChildren<Animator>();

        grunts = SoundLibrary.grunts;
        rayPadding = GetComponentInChildren<CircleCollider2D>().radius * .5f;

        gm = FindObjectOfType<GameMode>();

        fieldOfView = GetComponent<FieldOfView>();
        rigid = GetComponent<Rigidbody2D>();

        //fieldOfView.doUpdates = false;
        Cmd_Announce(gameObject);

        if (!hasAuthority) minimapPointLocal.enabled = false;

        Cmd_Start();
    }

    [Command]
    public void Cmd_Init()
    {
        if (hasAuthority)
        {
            minimapPoint.color = new Color(1, 1, 1);
            minimapPointLocal.enabled = true;
            //minimapThingy.enabled = true;

            Character[] foundChars = FindObjectsOfType<Character>();
            foreach (Character res in foundChars)
            {
                currentCharacters.Add(res);
            }
        }
        else
        {
            minimapPointLocal.enabled = false;
            minimapPoint.color = new Color(1, 0, 0);
            //minimapThingy.enabled = false;
        }
    }

    [ClientRpc]
    void Rpc_Init()
    {
        if (hasAuthority)
        {
            minimapPoint.color = new Color(1, 1, 1);
            minimapPointLocal.enabled = true;
            //minimapThingy.enabled = true;

            Character[] foundChars = FindObjectsOfType<Character>();
            foreach (Character res in foundChars)
            {
                currentCharacters.Add(res);
            }
        }
        else
        {
            minimapPointLocal.enabled = false;
            minimapPoint.color = new Color(1, 0, 0);
            //minimapThingy.enabled = false;
        }
    }

    [Command]
    void Cmd_UpdateTrans(float PosX, float PosY, float RotZ, float VelX, float VelY)
    {
        transform.position = new Vector3(PosX, PosY, transform.position.z);
        rigid.velocity = new Vector2(VelX, VelY);
        sprite.transform.eulerAngles = new Vector3(sprite.transform.eulerAngles.x, sprite.transform.eulerAngles.y, RotZ);

        Rpc_UpdateTransform(PosX, PosY, RotZ, VelX, VelY);
    }

    [ClientRpc]
    void Rpc_UpdateTransform(float PosX, float PosY, float RotZ, float VelX, float VelY)
    {
        if (hasAuthority || gameObject == null && rigid == null) return;

        transform.position = new Vector3(PosX, PosY, transform.position.z);
        rigid.velocity = new Vector2(VelX, VelY);
        sprite.transform.eulerAngles = new Vector3(sprite.transform.eulerAngles.x, sprite.transform.eulerAngles.y, RotZ);
    }

    protected virtual void Update()
    {
        if (isDashing)
        {
            transform.position = Vector2.MoveTowards(transform.position, dashTarget, dashSpeed * Time.deltaTime);

            if ((dashTarget - transform.position).magnitude < dashMiss)
            {
                dashSpeed = normalDashSpeed;
                isDashing = false;
            }
        }




    }

    private void FixedUpdate()
    {
        if (hasAuthority)
        {
            Cmd_UpdateTrans(transform.position.x, transform.position.y, sprite.transform.eulerAngles.z, rigid.velocity.x, rigid.velocity.y);
        }
    }

    [Command]
    protected virtual void Cmd_Start()
    {
        StartCoroutine(DisableInvurn());
    }

    IEnumerator DisableInvurn()
    {
        yield return new WaitForSeconds(respawnInvinciblity);
        invurnable = false;
    }

    [ClientRpc]
    protected virtual void RpcUpdate()
    {

    }

    public PlayerController GetPlayerController()
    {
        return owner;
    }

    public int GetPlayerIndex()
    {
        return playerIndex;
    }


    public void SetPlayerController(PlayerController pc)
    {
        owner = pc;
    }


    //for player attacks
    public void TakeDamage(float damage, GameObject killerO)
    {
        if (invurnable || isDead) return;
        Character killer = killerO.GetComponent<Character>();
        isDead = true;
        

        AudioSource a = Instantiate(diePlayer, transform.position, transform.rotation);

        a.gameObject.GetComponent<DieO>().StartSound(grunts[Random.Range(0, grunts.Length - 1)]);

        

        if (killer == this)
        {
            Cmd_DeductScore(killer.gameObject);
        }
        else
        {
            Cmd_GiveScore(killer.gameObject, gameObject);
        }
        
        

        if (!isLocalPlayer) return;
        
        GamePad.SetVibration(owner.GetPlayerIndex(), 0, 0);

    }

    [Command]
    void Cmd_Blood(float ang)
    {
        
    }

    [Command]
    void Cmd_DeductScore(GameObject kp)
    {
        Character k = kp.GetComponent<Character>();

        k.owner.Rpc_Suicided();

        if(gm.kills[k.owner] != 0)
        {
            gm.kills[k.owner]--;
        }
        
        Destroy(gameObject);
    }




    [Command]
    void Cmd_GiveScore(GameObject kp, GameObject dp)
    {

        Vector2 v2 = transform.position - kp.transform.position;
        float ang = Mathf.Atan2(v2.y, v2.x);
        
        for (int i = 0; i < 7; i++)
        {
            SpriteRenderer sr = Instantiate(blood, transform.position, Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360))));
            float lAng = ang + Random.Range(-30, 30);
            Vector2 vel = new Vector2(Mathf.Sin(lAng), Mathf.Cos(lAng)) * Random.Range(2, 6);
            sr.GetComponent<Rigidbody2D>().velocity = vel;
            sr.gameObject.transform.localScale = Vector3.one * 2;
        }

        Rpc_Blood(ang);

        Character k = kp.GetComponent<Character>();
        

        Character d = dp.GetComponent<Character>();
        k.owner.Rpc_Kill(d.owner.playerName);
        d.owner.Rpc_Die(k.owner.playerName);
        gm.kills[k.owner]++;

        //Rpc_Destroy(gameObject);
        Destroy(gameObject);
    }

    [ClientRpc]
    void Rpc_Blood(float ang)
    {
        for (int i = 0; i < 7; i++)
        {
            SpriteRenderer sr = Instantiate(blood, transform.position, Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360))));
            float lAng = ang + Random.Range(-30, 30);
            Vector2 vel = new Vector2(Mathf.Sin(lAng), Mathf.Cos(lAng)) * Random.Range(2, 6);
            sr.GetComponent<Rigidbody2D>().velocity = vel;
            sr.gameObject.transform.localScale = Vector3.one * 2;
        }

    }

    [ClientRpc]
    void Rpc_Destroy(GameObject go)
    {
        if (isServer) return;
        Destroy(go);
    }

    public virtual void Move(float x, float y)
    {
        rigid.velocity = new Vector2((x * speed), (y * speed));


    }


    public virtual void Rotate(float x, float y)
    {
        //float rotation = sprite.transform.eulerAngles.z;
        float rotation = Mathf.Atan2(-y, x) * Mathf.Rad2Deg;
        sprite.transform.eulerAngles = new Vector3(0, 0, -rotation - 90);
        //fieldOfView.currentZAngle = rotation;


    }

    [Command]
    private void Cmd_UpdateRotate(float rot)
    {
        fieldOfView.currentZAngle = rot;
    }


    //move the anchor for camera
    public void CameraPoint(float x, float y)
    {
        cameraPoint.transform.localPosition = Vector3.Lerp(cameraPoint.transform.localPosition, new Vector3(x * cameraAheadDist, y * cameraAheadDist, 0), Time.deltaTime);
    }

    //attack function, overriden in children
    public virtual void Attack()
    {

    }

    [Command]
    //turn on/off light
    public void Cmd_SwapLightStatus()
    {
        fov.enabled = !fov.enabled;

        fieldOfView.useCircleOnly = !fov.enabled;
        fovColor.enabled = fov.enabled;
        Rpc_SetLights(fov.enabled);

        if (!hasAuthority) return;
        minimapPointLocal.enabled = fov.enabled;

        
    }

    [ClientRpc]
    void Rpc_SetLights(bool state)
    {
        
        flashLightSounds.clip = state ? SoundLibrary.FLOn : SoundLibrary.FLOff;

        flashLightSounds.Play();
        fov.enabled = state;
        fovColor.enabled = state;
        fieldOfView.useCircleOnly = !state;

        if (!hasAuthority) return;
        
        minimapPointLocal.enabled = state;
    }


    //set layer to make sure the correct cameras render objects
    [Command]
    public void Cmd_SetPlayerLayer(int player)
    {
        area.gameObject.layer = 8 + player;

        playerIndex = player;

    }

    [ClientRpc]
    public void Rpc_SetPlayerLayer(int p)
    {
        area.gameObject.layer = 8 + p;

        playerIndex = p;
        if(hasAuthority) Cmd_SetPlayerLayer(p);
    }

    public void SetPlayerLayer(int p)
    {
        area.gameObject.layer = 8 + p;

        playerIndex = p;

        if (hasAuthority) Cmd_SetPlayerLayer(p);
    }

    //function for special move, overriden in all children using it

    public virtual void SpecialMove()
    {

    }

    public void Dash(float x, float y, float maxDist, bool otherData, bool specialCooldown)
    {
        if (isDashing) return;

        Vector2 dir = new Vector2(x, y);
        Vector2 ori = new Vector2(transform.position.x, transform.position.y);

        if (dir.magnitude <= .1f)
        {
            dir = new Vector2(sprite.transform.up.x, sprite.transform.up.y);
        }


        RaycastHit2D hit = Physics2D.Raycast(ori, dir, (maxDist + rayPadding), dashMask);



        Vector2 target;
        if (hit.collider != null)
        {
            target = hit.point;

            target -= ori;

            target = Vector2.ClampMagnitude(target, (target.magnitude - rayPadding));

            target += ori;

        }
        else
        {
            target = dir * maxDist;
            target = Vector2.ClampMagnitude(target, 1);
            target *= maxDist;
            target += ori;
        }


        Cmd_Dash(ori.x, ori.y, target.x, target.y, otherData, specialCooldown);
    }

    public void Dash(float x, float y)
    {
        if (isDashing) return;

        Vector2 dir = new Vector2(x, y);
        Vector2 ori = new Vector2(transform.position.x, transform.position.y);

        if (dir.magnitude <= .1f)
        {
            dir = new Vector2(sprite.transform.up.x, sprite.transform.up.y);
        }


        RaycastHit2D hit = Physics2D.Raycast(ori, dir, (DashMaxDistance + rayPadding), dashMask);



        Vector2 target;
        if (hit.collider != null)
        {
            target = hit.point;

            target -= ori;

            target = Vector2.ClampMagnitude(target, (target.magnitude - rayPadding));

            target += ori;

        }
        else
        {
            target = dir * DashMaxDistance;
            target = Vector2.ClampMagnitude(target, 1);
            target *= DashMaxDistance;
            target += ori;
        }


        Cmd_Dash(ori.x, ori.y, target.x, target.y, false, false);
    }

    [Command]
    void Cmd_Dash(float startX, float startY, float targetX, float targetY, bool otherData, bool SpecialCd)
    {
        if (dashCd && !SpecialCd) return;

        dashTarget = new Vector3(targetX, targetY, transform.position.z);
        transform.position = new Vector3(startX, startY, transform.position.z);
        if(!SpecialCd)
        {

            StartCoroutine(DashCd());
        }
        Rpc_Dash(startX, startY, targetX, targetY, otherData);
    }

    [ClientRpc]
    void Rpc_Dash(float startX, float startY, float targetX, float targetY, bool otherData)
    {
        isDashing = true;
        dashTarget = new Vector3(targetX, targetY, transform.position.z);
        transform.position = new Vector3(startX, startY, transform.position.z);

        if (otherData) SpecialDashFunction();
    }

    protected virtual void SpecialDashFunction()
    {
        
    }
    
    IEnumerator DashCd()
    {
        dashCd = true;
        yield return new WaitForSeconds(2);
        dashCd = false;
    }

    //haptic time
    public IEnumerator doHaptic(float time)
    {
        yield return new WaitForSeconds(time);
        GamePad.SetVibration(owner.GetPlayerIndex(), 0, 0);
    }

    protected IEnumerator AttackCooldown(float cd)
    {
        canAttack = false;
        yield return new WaitForSeconds(cd);
        canAttack = true;
    }
}
