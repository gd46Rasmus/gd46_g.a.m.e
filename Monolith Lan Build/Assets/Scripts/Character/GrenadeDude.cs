﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using XInputDotNetPure;

public class GrenadeDude : Character {

    public float bombcd = 1;
    public Grenade grenade;
    public float throwSpeed = 5;

    public float lastAttempt = 0;

    private Grenade currGrenade;

	public void ResetCd()
    {
        StartCoroutine(AttackCooldown(bombcd));
    }

    public override void Attack()
    {
        if(canAttack && lastAttempt < Time.time + .2f)
        {
            lastAttempt = Time.time;
            Cmd_Throw();
        }
    }

    [Command]
    void Cmd_Throw()
    {
        if(canAttack)
        {
            canAttack = false;
            Grenade spawn = Instantiate(grenade, projectileSpawner.transform.position, sprite.transform.rotation);
            spawn.dude = this;
            NetworkServer.SpawnWithClientAuthority(spawn.gameObject, GetPlayerController().gameObject);
            currGrenade = spawn;

            Rpc_Spawned(spawn.gameObject);

            anim.SetTrigger("tIsThrowing");
        }
    }
    
    public void Haptic()
    {
        if (!hasAuthority) return;
        GamePad.SetVibration(PlayerIndex.One, 1, 1);
        StartCoroutine(doHaptic(.7f));
    }
    

    [ClientRpc]
    void Rpc_Spawned(GameObject g)
    {
        currGrenade = g.GetComponent<Grenade>();
        g.GetComponent<Grenade>().dude = this;
        if(!isServer)
        anim.SetTrigger("tIsThrowing");
    }
    
    public override void SpecialMove()
    {
        Cmd_TriggerGrenade();
    }

    [Command]
    void Cmd_TriggerGrenade()
    {

        if (currGrenade != null)
        {
            currGrenade.Cmd_Explode();
        }
    }
}
