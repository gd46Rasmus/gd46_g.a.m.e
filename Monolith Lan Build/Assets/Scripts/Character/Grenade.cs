﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Grenade : NetworkBehaviour {

    public float time = 2;
    public float range = 1;
    public GrenadeDude dude;

    public SpriteRenderer gLight;
    public SpriteRenderer explosion;
    public SpriteRenderer grenade;

    public float explodeTime = .5f;

    private void Start()
    {

        GetComponent<Rigidbody2D>().velocity = (dude.GetComponent<Rigidbody2D>().velocity * .2f) + (new Vector2(dude.sprite.transform.up.x, dude.sprite.transform.up.y) * dude.throwSpeed);
        
        StartCoroutine(Timer());
    }

    [Command]
    public void Cmd_Explode()
    {
        explosion.gameObject.SetActive(true);
        gLight.enabled = false;
    }

    [Command]
    public void Cmd_Explodee()
    {
        gameObject.layer = 0;
        Rpc_Explode();

        GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;

        if (dude.gameObject == null) return;

        dude.ResetCd();
        dude.anim.SetTrigger("tIsDetonating");

    }

    [ClientRpc]
    void Rpc_Explode()
    {
        if(dude != null)
        {
            gameObject.layer = 0;
            Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, range, 1);


            for (int i = 0; i < cols.Length; i++)
            {
                Character c = cols[i].gameObject.GetComponentInParent<Character>();

                if (c != null)
                {
                    c.TakeDamage(1, dude.gameObject);
                }
            }
            dude.anim.SetTrigger("tIsDetonating");
            dude.ResetCd();

            dude.Haptic();

            GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            grenade.enabled = false;
            explosion.gameObject.SetActive(true);

            
        }
        else
        {
            Destroy(gameObject);
        }
    }


    public void Final()
    {
        Cmd_Delete();
    }

    [Command]
    void Cmd_Delete()
    {
        Destroy(gameObject);
        Rpc_Delete();
    }

    [ClientRpc]
    void Rpc_Delete()
    {
        Destroy(gameObject);
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(time);

        Cmd_Explode();
    }
    
}
