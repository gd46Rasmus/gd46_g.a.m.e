﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Melee : Character {

    public float meleeSpeed = 1.4f;

    bool meleeIsActive = false;
    [SyncVar] bool meleeReady = true;


    public float specialCd = 2f;
    public float specialDashSpeed = 6f;
    public float specialRange = 20;
    [SyncVar] bool specialReady = true;
    [SyncVar] bool specialLocked = false;

    public float swingAngle = 90;
    public float swingSpeed = 180;


    public void StopAttack()
    {
        Cmd_StopAttack();
    }

    [Command]
    void Cmd_StopAttack()
    {
        UpdateMelee(false);
    }

    
    void UpdateMelee(bool updatedVal)
    {
        if(isServer)
        {
            meleeIsActive = updatedVal;
            meleeObject.swordActive = updatedVal;
        }
        else
        {
            Cmd_UpdateMelee(updatedVal);
        }
    }

    [Command]
    void Cmd_UpdateMelee(bool updatedVal)
    {
        meleeIsActive = updatedVal;
        meleeObject.swordActive = updatedVal;
    }

    [ClientRpc]
    void Rpc_UpdateMelee(bool updatedVal)
    {
        meleeIsActive = updatedVal;
        meleeObject.swordActive = updatedVal;
    }

    public override void Move(float x, float y)
    {
        if(!specialLocked) base.Move(x, y);
    }

    protected override void Update()
    {
        base.Update();

        if(specialLocked && !isDashing)
        {
            anim.SetBool("bIsThrusting", false);

            specialLocked = false;
            UpdateMelee(false);
        }
    }

    

    //do a normal swing
    public override void Attack()
    {
        if(meleeReady && !meleeIsActive)
        {
            Cmd_Attack(true);
        }
        
    }

    //special move, spin attack
    public override void SpecialMove()
    {
        if(specialReady)
        {
            dashSpeed = specialDashSpeed;
            Dash(sprite.transform.up.x, sprite.transform.up.y, specialRange, true, true);
            Cmd_SpecialCd();
        }
    }

    [Command]
    void Cmd_SpecialCd()
    {
        StartCoroutine(SpecialCooldown(specialCd));
    }
   

    [ClientRpc]
    void Rpc_SetRot(float rot)
    {
        meleeObject.transform.eulerAngles = new Vector3(0, 0, rot);
    }

    protected override void SpecialDashFunction()
    {

        anim.SetBool("bIsThrusting", true);
        specialLocked = true;
        UpdateMelee(true);
    }
    
    
    [Command]
    void Cmd_Attack(bool normalAttack)
    {
        if(normalAttack)
        {
            anim.SetTrigger("tSwing");
            UpdateMelee(true);
            Sounds.clip = SoundLibrary.SwordSounds[Random.Range(0, SoundLibrary.SwordSounds.Length)];
            Sounds.Play();
            StartCoroutine(MeleeCooldown(meleeSpeed));
        }

        Rpc_Attack(normalAttack);
    }

    [ClientRpc]
    void Rpc_Attack(bool normalAttack)
    {
        if (normalAttack)
        {
            anim.SetTrigger("tSwing");
            Sounds.clip = SoundLibrary.SwordSounds[Random.Range(0, SoundLibrary.SwordSounds.Length)];
            Sounds.Play();
            StartCoroutine(MeleeCooldown(meleeSpeed));
        }
    }

    public override void Rotate(float x, float y)
    {
        if(!specialLocked)
        {
            base.Rotate(x, y);
        }
    }

    IEnumerator MeleeCooldown(float cd)
    {
        meleeReady = false;
        yield return new WaitForSeconds(cd);
        meleeReady = true;
    }

    IEnumerator SpecialCooldown(float cd)
    {
        specialReady = false;
        yield return new WaitForSeconds(cd);
        specialReady = true;
    }

}
