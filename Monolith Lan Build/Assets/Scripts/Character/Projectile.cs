﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Projectile : NetworkBehaviour {

    public float damage = 10;
    public float speed = 20;
    public float lifeSpan = .9f;

    [SyncVar] public GameObject gunner;

    // Use this for initialization
    void Start() {
        StartCoroutine(Die());
        GetComponent<Rigidbody2D>().velocity = transform.right * speed;
    }
    // Update is called once per frame
    void Update() {

    }

    void OnTriggerEnter2D(Collider2D otherActor)
    {

        Character hitActor = otherActor.gameObject.GetComponentInParent<Character>();


        if (hitActor != null && hitActor.gameObject != gunner)
        {
            hitActor.TakeDamage(damage, gunner);
            Destroy(gameObject);
        }

        if(hitActor == null)
        {
            Destroy(gameObject);
        }
        

       
        


    }

    protected IEnumerator Die()
    {
        yield return new WaitForSeconds(lifeSpan);
        Destroy(gameObject);
    }
}
