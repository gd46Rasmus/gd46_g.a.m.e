﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeDudeAnim : MonoBehaviour {

    public SpriteRenderer grenade;

	void HideGrenade()
    {
        grenade.enabled = false;
    }

    void ShowGrenade()
    {
        grenade.enabled = true;
    }
}
