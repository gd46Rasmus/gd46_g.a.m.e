﻿using System.Collections;
using System.Collections.Generic;

#if (UNITY_EDITOR)

using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class TileScript : MonoBehaviour
{

    public bool useCustomSnap = false;
    public bool useSnap = true;
    public bool UniformSnap = true;

    public float snapSize;

    public float snapX;
    public float snapY;

    public bool useRotationSnap = true;
    public float rotationSnapSize = 90;

    public bool copy = false;


    private void Start()
    {
        if (Application.isPlaying) Destroy(this);
    }

    void Update()
    {

        if (useSnap)
        {
            if (UniformSnap)
            {
                float x = transform.position.x / snapSize;
                x = Mathf.RoundToInt(x);
                x *= snapSize;

                float y = transform.position.y / snapSize;
                y = Mathf.RoundToInt(y);
                y *= snapSize;

                transform.position = new Vector3(x, y, transform.position.z);
            }
            else
            {
                float x = transform.position.x / snapX;
                x = Mathf.RoundToInt(x);
                x *= snapX;

                float y = transform.position.y / snapY;
                y = Mathf.RoundToInt(y);
                y *= snapY;

                transform.position = new Vector3(x, y, transform.position.z);
            }
        }
    }

    public void Rotate(bool isClockwise)
    {
        float z = transform.eulerAngles.z;

        if (isClockwise)
        {
            z -= rotationSnapSize;
        }
        else
        {
            z += rotationSnapSize;
        }

        transform.eulerAngles = new Vector3(0, 0, z);
    }

    public void Recalculate()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        if (sr != null)
        {
            snapX = sr.bounds.size.x;
            snapY = sr.bounds.size.y;

            snapSize = snapX;
        }

    }

    public TileScript Move(float x, float y)
    {

        float nx, ny;
        if (UniformSnap)
        {
            nx = transform.position.x + (x * snapSize);
            ny = transform.position.y + (y * snapSize);
        }
        else
        {
            nx = transform.position.x + (x * snapX);
            ny = transform.position.y + (y * snapY);
            
        }
        
        if(copy)
        {
            GameObject clone = PrefabUtility.InstantiatePrefab(PrefabUtility.GetPrefabParent(gameObject)) as GameObject;
            

            TileScript cr = clone.GetComponent<TileScript>();

            cr.transform.rotation = transform.rotation;

            cr.copy = copy;
            cr.transform.parent = transform.parent;
            cr.transform.position = new Vector3(nx, ny, transform.position.z);
            return cr;
        }
        else
        {
            transform.position = new Vector3(nx, ny, transform.position.z);
        }
        return null;

    }
}


#endif