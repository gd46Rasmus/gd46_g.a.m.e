﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using XInputDotNetPure;

public class UIManager : MonoBehaviour {

    public Image[] HUD;

    public GameObject Pause;

    public Image meleeIcon;
    public Image rangerIcon;
    public Image assassinIcon;

    public PlayerController pc;

    private GameMode gm;

    public Text killedText;

    public Text stats;
    public Text time;
    private UIState currentState = UIState.Select;

    bool hasStarted = false;

	// Use this for initialization
	void Start () {
        UpdateUI();

        gm = FindObjectOfType<GameMode>();

    }
	
    public UIState GetState()
    {
        return currentState;
    }

	// Update is called once per frame
	void Update () {
        time.text = Mathf.FloorToInt(GameMode.GetTimeLeft() / 60) + "." + Mathf.FloorToInt(GameMode.GetTimeLeft() % 60);
        string statsText = "";
        foreach(KeyValuePair<PlayerController, uint> pck in gm.kills)
        {
            statsText += pck.Key.playerName + " : " + pck.Value + "\n";
        }
        stats.text = statsText;

        if(GameMode.inGame && !hasStarted)
        {
            hasStarted = true;

            killedText.enabled = false;
        }

        if (!hasStarted)
        {
            killedText.enabled = true;
            killedText.text = "Waiting for players (" + (FindObjectsOfType<PlayerController>().Length) + "/4)";
            currentState = UIState.Waiting;
            UpdateUI();
            if (pc.isServer && gm.GetPlayers() > 2)
            {
                killedText.text = killedText.text + "\nPress A to start game";
            }
        }
    }

    public void UpdateUI()
    {
        //non frame to frame updates of ui
        switch(currentState)
        {
            case UIState.Select:
                foreach (Image i in HUD)
                {
                    i.enabled = false;
                }

                meleeIcon.color = new Color(1, 1, 1, 1);
                rangerIcon.color = new Color(1, 1, 1, 1);
                assassinIcon.color = new Color(1, 1, 1, 1);

                StartCoroutine(StartHaptic());

                break;
            case UIState.InGame:

                foreach (Image i in HUD)
                {
                    i.enabled = true;
                }

                meleeIcon.color = new Color(1, 1, 1, 0);
                rangerIcon.color = new Color(1, 1, 1, 0);
                assassinIcon.color = new Color(1, 1, 1, 0);
                Pause.SetActive(false);
                break;
            case UIState.Waiting:
                meleeIcon.color = new Color(1, 1, 1, 0);
                rangerIcon.color = new Color(1, 1, 1, 0);
                assassinIcon.color = new Color(1, 1, 1, 0);

                foreach(Image i in HUD)
                {
                    i.enabled = false;
                }
                Pause.SetActive(false);
                break;

            case UIState.Pause:
                meleeIcon.color = new Color(1, 1, 1, 0);
                rangerIcon.color = new Color(1, 1, 1, 0);
                assassinIcon.color = new Color(1, 1, 1, 0);

                foreach (Image i in HUD)
                {
                    i.enabled = false;
                }

                Pause.SetActive(true);
                break;

            default:
                break;
        }
    }

    public void SetPlayer(int playerIndex, Camera playerCam)
    {
        //set layer for ui, define what player sees it
        gameObject.layer = 1;
        GetComponentInParent<Canvas>().worldCamera = playerCam;
    }

    public void SetState(UIState newState)
    {
        currentState = newState;
    }

    public void PlayerKilled(string playerId)
    {
        killedText.text = playerId + " killed you";
        StartCoroutine(Hide());
    }

    public void KillPlayer(string playerId)
    {
        killedText.text = "You killed " + playerId;
        StartCoroutine(Hide());
    }

    public void Suicided()
    {
        killedText.text = "You commited suicide";
        StartCoroutine(Hide());
    }

    public void StartGame(int p)
    {
        killedText.text = "You are Player " + p;
    }

    IEnumerator Hide()
    {

        killedText.enabled = true;
        yield return new WaitForSeconds(2);
        killedText.enabled = false;
    }

    
    public enum UIState { Select, InGame, Waiting, Pause}

    public void PauseMenu()
    {
        if(currentState != UIState.Pause)
        {
            currentState = UIState.Pause;
            UpdateUI();
        }
        else
        {
            Resume();
        }
    }

    public void Resume()
    {
        if (pc.pawn != null)
        {
            currentState = UIState.InGame;
        }
        else
        {
            currentState = UIState.Select;
        }

        UpdateUI();
    }

    public void LeaveButton()
    {
        pc.Leave();
    }

    IEnumerator StartHaptic()
    {
        GamePad.SetVibration(pc.playerIndex, .6f, .6f);

        yield return new WaitForSeconds(.5f);
        GamePad.SetVibration(pc.playerIndex, 0, 0);

    }
}
