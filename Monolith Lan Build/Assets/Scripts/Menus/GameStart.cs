﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameStart : MonoBehaviour {


    public Text errorMsg;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Return))
        {
            TestPlayerName();
        }
    }

    public void TestPlayerName()
    {
        
        InputField i = FindObjectOfType<InputField>();

        if (i.text != null && i.text != "")
        {
            if(i.text.Length <= 16)
            {
                ComputerStatics.SetName(i.text);
                SceneManager.LoadScene("Lobby");                
            }
            else
            {
                errorMsg.text = "Name to long. Max 16 characters";
                errorMsg.enabled = true;
            }
        }
        else
        {
            errorMsg.text = "Please enter a name";
            errorMsg.enabled = true;
        }
    }    
}
