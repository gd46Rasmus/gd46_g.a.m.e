﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndScreen : MonoBehaviour {

    public Text[] Kills;
    public Text[] Names;
    public Image[] Bloods;
    
    GameMode gm;
    

    void Start ()
    {
        XInputDotNetPure.GamePad.SetVibration(XInputDotNetPure.PlayerIndex.One, 0, 0);

        gm = FindObjectOfType<GameMode>();
        gm.endIsLoaded = true;
        GameMode.inGame = false;
        int i = 0;
        uint killSum = 0;
        uint[] kills = new uint[gm.kills.Count];
        string[] names = new string[gm.kills.Count];

        foreach (KeyValuePair<PlayerController, uint> v in gm.kills)
        {
            kills[i] = v.Value;
            names[i] = v.Key.playerName;
            killSum += v.Value;
            i++;
        }

        killSum = killSum == 0 ? 1 : killSum;
        

        for(i = 0; i < kills.Length; i++)
        {
            Kills[i].text = ((int)(((float)kills[i] / (float)killSum) * 100)).ToString() + "%";
           
            Names[i].text = names[i];
            Bloods[i].rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, ((float)kills[i] / (float)killSum) * 270f);
        }
    }
	
	public void ToMenu()
    {
        if(gm.isServer)
        {
            FindObjectOfType<NetManager>().StopHost();
        }
        else
        {
            Network.Disconnect();
        }
        foreach(PlayerController pc in FindObjectsOfType<PlayerController>())
        {
            Destroy(pc.gameObject);
        }
        Destroy(gm.gameObject);
        SceneManager.LoadScene("Lobby");
    }
}
