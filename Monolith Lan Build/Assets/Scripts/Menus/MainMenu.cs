﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public enum MenuState { Main, ServerList, Settings, Credits };

    public GameObject serverList;
    public GameObject main;
    public GameObject credits;
    public GameObject list;

    public NetManager nm;
	// Use this for initialization
	void Start () {
        nm = FindObjectOfType<NetManager>();

        nm.discoverer.list = list;
    }

    void ChangeState(MenuState newState)
    {
        switch(newState)
        {
            case MenuState.Main:
                nm.StopSearch();
                credits.SetActive(false);
                serverList.SetActive(false);
                main.SetActive(true);
                nm.discoverer.StopSearch();
                break;
            case MenuState.ServerList:
                credits.SetActive(false);
                main.SetActive(false);
                serverList.SetActive(true);
                nm.discoverer.StartSearch();
                break;
            case MenuState.Credits:
                credits.SetActive(true);
                main.SetActive(false);
                serverList.SetActive(false);
                break;
        }
    }

    public void SLBack()
    {
        ChangeState(MenuState.Main);
        nm.StopSearch();
    }

    public void MMList()
    {
        ChangeState(MenuState.ServerList);
    }

    public void MMHost()
    {
        NetworkTransport.Init();
        ConnectionConfig cConfig = new ConnectionConfig();
        cConfig.SendDelay = 0;

        cConfig.AddChannel(QosType.Reliable);
        cConfig.AddChannel(QosType.Unreliable);


        nm.StartHost(cConfig, 32);
    }

    public void MMExit()
    {
        Application.Quit();
    }
}
