﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.UI;

public class ListEntry : MonoBehaviour {

    public string ip;
    public string sName;
    public Text textField;
    NetManager net;

    private void Start()
    {
        net = FindObjectOfType<NetManager>();
    }

    public void UpdateText()
    {
        textField.text = sName;
    }

    public void Connect()
    {
        net.networkAddress = ip;
        net.networkPort = 7777;
        net.StopSearch();
        net.StartClient();
    }
}
