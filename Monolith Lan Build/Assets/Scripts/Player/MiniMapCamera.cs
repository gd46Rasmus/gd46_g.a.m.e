﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapCamera : MonoBehaviour {

    public Character ownChar;

    public float updateInterval = .3f;

    public float hideRange;
    

    void NewChar(Character newChar)
    {
        ownChar = newChar;
    }

    private void Start()
    {
        StartCoroutine(Check());
    }

    IEnumerator Check()
    {
        while (true)
        {

            if(ownChar != null)
            {

            }

            yield return new WaitForSeconds(updateInterval);
        }
    }
}
