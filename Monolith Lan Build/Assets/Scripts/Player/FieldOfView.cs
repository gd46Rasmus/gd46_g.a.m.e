﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
///     By: Sebastian Lague
///     Edited by: Rasmus Jakobsson    
/// 
/// 
///     Source: https://github.com/SebLague/Field-of-View
/// </summary>

public class FieldOfView : NetworkBehaviour
{



    //.9 is a yellow scale in the material
    public float vCord = .9f;

    public float viewRadius;

    public float circleRadius;

    [Range(0, 360)]
    public float viewAngle;

    [SyncVar] public float currentZAngle;

    public GameObject sprite;

    public LayerMask obstacleMask;

    public PolygonCollider2D trigger;

    public float meshResolution;
    public int edgeResolveIterations;
    public float edgeDstThreshold;

    public float maskCutawayDst = .1f;

    [SyncVar] public bool doUpdates = true;

    public MeshFilter viewMeshFilter;
    public MeshFilter colorMeshFilter;

    public MeshFilter circleMeshFilter;

    [HideInInspector]
    public bool useCircleOnly = false;

    Mesh colorMesh;
    Mesh viewMesh;
    Mesh circleMesh;

    void Start()
    {
        viewMesh = new Mesh();
        viewMesh.name = "View Mesh";
        viewMeshFilter.mesh = viewMesh;

        if(circleMeshFilter != null)
        {
            circleMesh = new Mesh();
            circleMesh.name = "Cricle Mesh";
            circleMeshFilter.mesh = circleMesh;
        }

        if(colorMeshFilter != null)
        {
            colorMesh = new Mesh();
            colorMesh.name = "Color Mesh";
            colorMeshFilter.mesh = colorMesh;
        }
    }

    

    [Command]
    void Cmd_Update(bool state)
    {
        doUpdates = state;
    }

    void LateUpdate()
    {

        if (doUpdates)
        {
            currentZAngle = sprite.transform.eulerAngles.z + 90;

            DrawFieldOfView();
        }
    }

    void DrawFieldOfView()
    {
        float lViewAngle = 0;
        float lRange = 0;
        if (useCircleOnly)
        {
            if (!hasAuthority) return;
            lViewAngle = 360;
            lRange = circleRadius;
        }
        else
        {
            lViewAngle = viewAngle;
            lRange = viewRadius;
        }

        int stepCount = Mathf.RoundToInt(lViewAngle * meshResolution);
        float stepAngleSize = lViewAngle / stepCount;
        List<Vector2> viewPoints = new List<Vector2>();
        List<float> viewAngles = new List<float>();
        ViewCastInfo oldViewCast = new ViewCastInfo();
         

        //cone calculation
        for (int i = 0; i <= stepCount; i++)
        {
            float angle = currentZAngle - lViewAngle / 2 + stepAngleSize * i;
            
            ViewCastInfo newViewCast = ViewCast(angle, lRange);

            if (i > 0)
            {
                bool edgeDstThresholdExceeded = Mathf.Abs(oldViewCast.dst - newViewCast.dst) > edgeDstThreshold;
                if (oldViewCast.hit != newViewCast.hit || (oldViewCast.hit && newViewCast.hit && edgeDstThresholdExceeded))
                {
                    EdgeInfo edge = FindEdge(oldViewCast, newViewCast, lRange);
                    if (edge.pointA != Vector2.zero)
                    {
                        viewPoints.Add(edge.pointA);
                        viewAngles.Add(edge.angleA);
                    }
                    if (edge.pointB != Vector2.zero)
                    {
                        viewPoints.Add(edge.pointB);
                        viewAngles.Add(edge.angleB);
                    }
                }

            }


            viewPoints.Add(newViewCast.point);
            viewAngles.Add(newViewCast.angle);
            oldViewCast = newViewCast;
        }
        

        int vertexCount = viewPoints.Count + 1;
        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];
        vertices[0] = Vector3.zero;

        Vector2[] uvs = new Vector2[viewPoints.Count + 1];
        
        //build cone
        for (int i = 0; i < vertexCount - 1; i++)
        {
            Vector2 dir = DirFromAngle(viewAngles[i], true);
            Vector3 forward = new Vector3(dir.x, dir.y);

            //Vector3 v3 = new Vector3(viewPoints[i].x - transform.position.x, viewPoints[i].y - transform.position.y, 0);
            Vector3 v3 = transform.InverseTransformPoint(viewPoints[i]) + (forward * maskCutawayDst);
            v3 = Vector3.ClampMagnitude(v3, viewRadius);
            vertices[i + 1] = v3;

            float u = (v3.magnitude / viewRadius);

            uvs[i+1] = new Vector2(u, vCord);

            if (i < vertexCount - 2)
            {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }
        }
        if(!useCircleOnly)
        {
            viewMesh.Clear();
            viewMesh.vertices = vertices;
            viewMesh.triangles = triangles;
            viewMesh.RecalculateNormals();

            if(trigger != null)
            {
                List<Vector2> listVerticies = new List<Vector2>();
                listVerticies.Add(vertices[0]);
                for (int i = 1; i < vertices.Length; i++)
                {
                    if (vertices[i] != Vector3.zero)
                    {
                        listVerticies.Add(vertices[i]);
                    }
                }

                trigger.points = listVerticies.ToArray();
            }
        }
        else
        {
            circleMesh.Clear();
            circleMesh.vertices = vertices;
            circleMesh.triangles = triangles;
            circleMesh.RecalculateNormals();
        }
        

        //color uses the data from normal cone
        if(colorMesh != null && !useCircleOnly)
        {


            uvs[0] = new Vector2(0, vCord);

            colorMesh.Clear();

            colorMesh.vertices = vertices;
            colorMesh.triangles = triangles;
            colorMesh.RecalculateNormals();
            colorMesh.uv = uvs;
        }

        //calculate extra circle
        //finish here if it only used circle as double circles are redundant
        if (useCircleOnly || !hasAuthority) return;


        lRange = circleRadius;

        lViewAngle = 360 - viewAngle;

        stepCount = Mathf.RoundToInt(lViewAngle * meshResolution);
        stepAngleSize = lViewAngle / stepCount;
        viewPoints.Clear();
        viewAngles.Clear();
        oldViewCast = new ViewCastInfo();

        for (int i = 0; i <= stepCount; i++)
        {
            float angle = currentZAngle + (viewAngle / 2) + stepAngleSize * i;

            ViewCastInfo newViewCast = ViewCast(angle, lRange);

            if (i > 0)
            {
                bool edgeDstThresholdExceeded = Mathf.Abs(oldViewCast.dst - newViewCast.dst) > edgeDstThreshold;
                if (oldViewCast.hit != newViewCast.hit || (oldViewCast.hit && newViewCast.hit && edgeDstThresholdExceeded))
                {
                    EdgeInfo edge = FindEdge(oldViewCast, newViewCast, lRange);
                    if (edge.pointA != Vector2.zero)
                    {
                        viewPoints.Add(edge.pointA);
                        viewAngles.Add(edge.angleA);
                    }
                    if (edge.pointB != Vector2.zero)
                    {
                        viewPoints.Add(edge.pointB);
                        viewAngles.Add(edge.angleB);
                    }
                }

            }


            viewPoints.Add(newViewCast.point);
            viewAngles.Add(newViewCast.angle);
            oldViewCast = newViewCast;
        }

        vertexCount = viewPoints.Count + 1;
        vertices = new Vector3[vertexCount];
        triangles = new int[(vertexCount - 2) * 3];
        vertices[0] = Vector3.zero;
        
        for (int i = 0; i < vertexCount - 1; i++)
        {

            Vector2 dir = DirFromAngle(viewAngles[i], true);
            Vector3 forward = new Vector3(dir.x, dir.y);

            //Vector3 v3 = new Vector3(viewPoints[i].x - transform.position.x, viewPoints[i].y - transform.position.y, 0);
            Vector3 v3 = transform.InverseTransformPoint(viewPoints[i]) + (forward * maskCutawayDst);
            v3 = Vector3.ClampMagnitude(v3, viewRadius);
            vertices[i + 1] = v3;
            

            if (i < vertexCount - 2)
            {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }
        }

        circleMesh.Clear();
        circleMesh.vertices = vertices;
        circleMesh.triangles = triangles;
        circleMesh.RecalculateNormals();

        

    }

    EdgeInfo FindEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast, float range)
    {
        float minAngle = minViewCast.angle;
        float maxAngle = maxViewCast.angle;
        Vector2 minPoint = Vector2.zero;
        Vector2 maxPoint = Vector2.zero;

        for (int i = 0; i < edgeResolveIterations; i++)
        {
            float angle = (minAngle + maxAngle) / 2;
            ViewCastInfo newViewCast = ViewCast(angle, range);

            bool edgeDstThresholdExceeded = Mathf.Abs(minViewCast.dst - newViewCast.dst) > edgeDstThreshold;
            if (newViewCast.hit == minViewCast.hit && !edgeDstThresholdExceeded)
            {
                minAngle = angle;
                minPoint = newViewCast.point;
            }
            else
            {
                maxAngle = angle;
                maxPoint = newViewCast.point;
            }
        }

        return new EdgeInfo(minPoint, maxPoint, minAngle, maxAngle);
    }


    ViewCastInfo ViewCast(float globalAngle, float range)
    {
        Vector3 dir = DirFromAngle(globalAngle, true);
        RaycastHit2D hit;

        

        if (hit = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), dir, range, obstacleMask))
        {
            return new ViewCastInfo(true, hit.point, hit.distance, globalAngle);
        }
        else
        {
            return new ViewCastInfo(false, transform.position + dir * range, range, globalAngle);
        }
    }

    public Vector2 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.z;
        }
        return new Vector2(Mathf.Cos(angleInDegrees * Mathf.Deg2Rad), Mathf.Sin(angleInDegrees * Mathf.Deg2Rad));
    }
    


    public struct ViewCastInfo
    {
        public bool hit;
        public Vector2 point;
        public float dst;
        public float angle;

        public ViewCastInfo(bool _hit, Vector2 _point, float _dst, float _angle)
        {
            hit = _hit;
            point = _point;
            dst = _dst;
            angle = _angle;
        }
    }

    public struct EdgeInfo
    {
        public Vector2 pointA;
        public Vector2 pointB;
        public float angleA;
        public float angleB;

        public EdgeInfo(Vector2 _pointA, Vector2 _pointB, float _angleA, float _angleB)
        {
            pointA = _pointA;
            pointB = _pointB;
            angleA = _angleA;
            angleB = _angleB;
        }
    }

}
