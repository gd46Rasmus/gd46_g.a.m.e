﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using XInputDotNetPure;

public class PlayerController : NetworkBehaviour {

    public List<GameObject> CharactersAvailable = new List<GameObject>();

    public UIManager ui;

    [SyncVar] public string playerName;
    
    public PlayerIndex playerIndex;
    GamePadState state;
    GamePadState prevState;

    public GameObject playerCamera;
    public Camera minimapCamera;

    [SyncVar(hook = "UpdatePlayerN")] public int playerN = 1;

    public float baseRespawnTime = 10;

    private float lastSpawnAttempt = -10;
    private float spawnAttemptDelay = 1.5f;

    [SyncVar] private Vector3 meleePos;

    private PlayerController[] players;
    private Character activePawn;

    public float invisRange = 100;
    public float obliqueRange = 200;

    public Vector2 rightStick
    {
        get
        {
            return new Vector2(state.ThumbSticks.Right.X, state.ThumbSticks.Right.Y);
        }
    }

    public Character pawn
    {
        get
        {
            return activePawn;
        }
    }

    public bool Attacking
    {
        get
        {
            return (state.Triggers.Right > .4f);
        }
    }

    public void Leave()
    {
        if(isClient)
        {
            Network.Disconnect();
        }
        else if(isServer)
        {
            NetworkServer.DisconnectAll();
        }
        

    }

    // Use this for initialization
    void Start () {
        meleePos = transform.position;



        //create a camera
        if (!isLocalPlayer) return;

        Cmd_SetPlayerName(ComputerStatics.Name);

        playerCamera = Instantiate(playerCamera, transform.position, transform.rotation);
        Instantiate(minimapCamera);
        Cmd_Start();
        playerCamera.GetComponent<CameraController>().SetPlayer(playerN, 3);

        ui = Instantiate(ui, transform.position, transform.rotation);
        ui.pc = this;
        ui.GetComponent<Canvas>().worldCamera = playerCamera.GetComponent<Camera>();

        
        
        //set the camera culling mask depending on the player



        //PlayerIndex testPlayerIndex = (PlayerIndex)(playerN - 1);


        ui.SetPlayer(playerN, playerCamera.GetComponent<Camera>());
    }

    [Command]
    void Cmd_Start()
    {
        FindObjectOfType<GameMode>().Cmd_GetPlayerIndex(gameObject);
    }

    [Command]
    void Cmd_SetPlayerName(string name)
    {
        playerName = name;
    }
    
    void UpdatePlayerN(int updatedPlayer)
    {

        playerN = updatedPlayer;
        if (!isLocalPlayer) return;
        
        switch (playerN)
        {
            case 1:
                playerCamera.GetComponent<Camera>().cullingMask = -138241;
                break;
            case 2:
                playerCamera.GetComponent<Camera>().cullingMask = -137729;
                break;
            case 3:
                playerCamera.GetComponent<Camera>().cullingMask = -136705;
                break;
            case 4:
                playerCamera.GetComponent<Camera>().cullingMask = -134657;
                break;
        }
    }

    public void Explosion()
    {
        StartCoroutine(haptic());
    }

    IEnumerator haptic()
    {
        GamePad.SetVibration(playerIndex, 1, 1);
        yield return new WaitForSeconds(1.3f);
        GamePad.SetVibration(playerIndex, 0, 0);
    }

    [ClientRpc]
    public void Rpc_MSG()
    {
        if (ui != null)
        {
            ui.gameObject.SetActive(true);
            ui.enabled = true;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update() {

        //dont run input from external players
        if (!isLocalPlayer) return;


        //states of gamepad for controls
        prevState = state;
        state = GamePad.GetState(playerIndex);

        if (isServer && !GameMode.inGame && Input.GetKeyDown(KeyCode.O))
        {

            FindObjectOfType<GameMode>().ForceStart();
        }

        if (isServer && Input.GetKeyDown(KeyCode.P))
        {
            FindObjectOfType<GameMode>().kills[this] += 1;
        }

        if (isServer && !GameMode.inGame && FindObjectOfType<GameMode>().GetPlayers() > 2 && state.Buttons.A == ButtonState.Pressed)
        {
            FindObjectOfType<GameMode>().ForceStart();
        }

        if(Input.GetKeyDown(KeyCode.L))
        {
            if(activePawn != null) activePawn.TakeDamage(1, activePawn.gameObject);
        }

        if (!GameMode.inGame) return;

        if(state.Buttons.Start == ButtonState.Pressed && prevState.Buttons.Start == ButtonState.Released)
        {
            ui.PauseMenu();
        }

        Vector3 pos;
        if (activePawn != null)
        {
            pos = activePawn.transform.position;
        }
        else
        {
            pos = transform.position;
        }

        
        if(players != null)
        {
            foreach (PlayerController pc in players)
            {

                Character c = pc.activePawn;

                if (c != null)
                {

                    float dist = (c.transform.position - pos).magnitude;

                    Color co = c.minimapColor;

                    if (dist < invisRange)
                    {
                        co.a = 0;
                    }
                    else if (dist > obliqueRange)
                    {
                        co.a = 1;
                    }
                    else
                    {
                        float r = dist - invisRange;
                        float m = obliqueRange - invisRange;

                        float alpha = Mathf.Lerp(1, 0, 1 - (r / m));

                        co.a = alpha;
                    }
                    c.minimapPoint.color = co;
                }
            }
        }
        else
        {
            players = FindObjectsOfType<PlayerController>();
        }
        

        //check if controlling a pawn
        if (activePawn != null)
        {
            //normal inputs
            if (state.Triggers.Right > .4f && prevState.Triggers.Right <= .4f) 
            {
                activePawn.Attack();
            }


            if (state.ThumbSticks.Right.X != 0 || state.ThumbSticks.Right.Y != 0)
            {
                activePawn.Rotate(state.ThumbSticks.Right.X, state.ThumbSticks.Right.Y);
                
            }

            activePawn.CameraPoint(state.ThumbSticks.Right.X, state.ThumbSticks.Right.Y);
            
            if (Mathf.Abs(state.ThumbSticks.Left.X) > 0.15 || Mathf.Abs(state.ThumbSticks.Left.Y) >= 0.15)
            {
                activePawn.Move(state.ThumbSticks.Left.X, state.ThumbSticks.Left.Y);
                activePawn.anim.SetBool("bIsRunning", true);
            }
            else
            {
                activePawn.anim.SetBool("bIsRunning", false);
            }

            if (state.Triggers.Left > .4f && prevState.Triggers.Left <= .4f)
            {
                activePawn.Dash(state.ThumbSticks.Left.X, state.ThumbSticks.Left.Y);
            }

            if (state.Buttons.LeftShoulder == ButtonState.Pressed && prevState.Buttons.LeftShoulder == ButtonState.Released)
            {
                activePawn.Cmd_SwapLightStatus();
            }
            if (state.Buttons.RightShoulder == ButtonState.Pressed && prevState.Buttons.RightShoulder == ButtonState.Released)
            {
                activePawn.SpecialMove();
            }
        }
        else 
        {
            if(ui.GetState() != UIManager.UIState.Select)
            {
                ui.SetState(UIManager.UIState.Select);
                ui.UpdateUI();
            }
            

            if (state.Buttons.X == ButtonState.Pressed && (lastSpawnAttempt + spawnAttemptDelay <= Time.time))
            {
                lastSpawnAttempt = Time.time;
                ui.SetState(UIManager.UIState.InGame);
                Cmd_Spawn(0);
                ui.UpdateUI();
            }
            else if(state.Buttons.Y == ButtonState.Pressed && (lastSpawnAttempt + spawnAttemptDelay <= Time.time))
            {
                lastSpawnAttempt = Time.time;
                ui.SetState(UIManager.UIState.InGame);
                Cmd_Spawn(1);
                ui.UpdateUI();
            }
            else if(state.Buttons.B == ButtonState.Pressed && (lastSpawnAttempt + spawnAttemptDelay <= Time.time))
            {
                lastSpawnAttempt = Time.time;
                ui.SetState(UIManager.UIState.InGame);
                Cmd_Spawn(2);
                ui.UpdateUI();
            }
        }
    }
    
    public void GameStart()
    {
        if (!hasAuthority) return;

        players = FindObjectsOfType<PlayerController>();
    }

    [Command]
    private void Cmd_Spawn(int charId)
    {
        GameObject spawn = Instantiate(CharactersAvailable[charId], meleePos, new Quaternion());

        
        
        spawn.GetComponent<Character>().SetPlayerController(this);

        NetworkServer.SpawnWithClientAuthority(spawn, gameObject);
        spawn.GetComponent<Character>().Rpc_SetPlayerLayer(playerN);
        Rpc_Spawn(spawn);
    }

    [ClientRpc]
    void Rpc_Spawn(GameObject spawned)
    {
        spawned.GetComponent<Character>().SetPlayerController(this);

        activePawn = spawned.GetComponent<Character>();
   

        spawned.GetComponent<Character>().SetPlayerLayer(playerN);

        if (!isLocalPlayer) return;

        playerCamera.GetComponent<CameraController>().NewTarget(activePawn);
        ui.SetState(UIManager.UIState.InGame);
        ui.UpdateUI();

        activePawn.Cmd_Init();
    }
    

    public PlayerIndex GetPlayerIndex()
    {
        return playerIndex;
    }

    [ClientRpc]
    public void Rpc_Suicided()
    {
        ui.Suicided();
    }

    [ClientRpc]
    public void Rpc_Kill(string player)
    {
        ui.KillPlayer(player);
    }

    [ClientRpc]
    public void Rpc_Die(string player)
    {
        ui.PlayerKilled(player);
    }
}