﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    private Character target;
    public float cameraZoom = 280;

	// Update is called once per frame
	void Update () {


        if (target != null)
        {
            Vector3 position = target.cameraPoint.transform.position;

            position.z -= 10;

            transform.position = position;
        }
	}

    public void NewTarget(Character target)
    {
        this.target = target;
        GetComponent<Camera>().orthographicSize = cameraZoom;
    }

    public void SetPlayer(int player, int playerCount)
    {
        Camera cam = GetComponent<Camera>();

       

        cam.rect = new Rect(0,0,1,1);

    }
}
