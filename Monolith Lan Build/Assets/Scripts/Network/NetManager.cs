﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetManager : NetworkManager {

    public Discoverer discoverer;

    public override void OnStartHost()
    {
        //discoverer.broadcastData = ComputerStatics.Name + "'s Server";
        //discoverer.Initialize();
        //discoverer.StartAsServer();

        discoverer.StartHost();
    }
    

    public override void OnStopClient()
    { 
        
    }

    public void StartSearch()
    {
        //discoverer.Initialize();
        //discoverer.StartAsClient();

        discoverer.StartSearch();
    }


    public void StopSearch()
    {
        discoverer.StopSearch();

        //discoverer.StopBroadcast();
    }
}
