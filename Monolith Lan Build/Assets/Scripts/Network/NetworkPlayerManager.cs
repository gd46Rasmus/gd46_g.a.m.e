﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkPlayerManager : NetworkBehaviour {

    int AvailableIds = 4;
    static List<int> ids;

	void Start () {
        ids = new List<int>();

        for(int i = 0; i < AvailableIds; i++)
        {
            ids.Add(i);
        }
	}

    private void OnPlayerDisconnected(NetworkPlayer player)
    {
        PlayerController[] pc = FindObjectsOfType<PlayerController>();

        for (int i = 0; i < AvailableIds; i++)
        {
            ids.Add(i);
        }

        foreach(PlayerController p in pc)
        {
            
        }

    }

    public int GetOwnPlayerId(NetworkPlayer player)
    {
        return 0;
    }

}
