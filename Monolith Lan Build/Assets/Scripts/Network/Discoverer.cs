﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using Mono.Data.Sqlite;
using UnityEngine;
using UnityEngine.Networking;
using System.Text.RegularExpressions;

public class Discoverer : NetworkDiscovery {

    bool isSearching = false;

    IDbCommand dbcmd;
    IDataReader reader;
    IDbConnection dbconn;

    public ListEntry entry;
    public GameObject list;
    Dictionary<string, ListEntry> servers = new Dictionary<string, ListEntry>();

    private string DbLoc = "URI=file:\\\\vfsstorage10\\GameDesign\\Students\\Shared_Student_Storage\\GD46\\Monolith\\Sqlite\\MonoServers.db";

    private void Start()
    {
        list = FindObjectOfType<MainMenu>().list;

        
    }

    public override void OnReceivedBroadcast(string fromAddress, string data)
    {
        base.OnReceivedBroadcast(fromAddress, data);
        string ip = Regex.Match(fromAddress, "[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}").Value;

        if(!servers.ContainsKey(ip))
        {
            servers.Add(ip, AddServer(ip, data));
        }
    }

    ListEntry AddServer(string ip, string data)
    {
        
        ListEntry l = Instantiate(entry, list.transform);

        l.ip = ip;
        l.sName = data;
        l.UpdateText();

        return l;
    }

    public void StartSearch()
    {
        isSearching = true;

        StartCoroutine(Search());
    }

    public void StopSearch()
    {
        isSearching = false;
    }

    IEnumerator Search()
    {
        while(isSearching)
        {

            Dictionary<string, string> Servers = new Dictionary<string, string>();

            System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
            int curTime = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;

            
            dbconn = (IDbConnection)new SqliteConnection(DbLoc);
            dbconn.Open(); //Open connection to the database.
            dbcmd = dbconn.CreateCommand();
            string sqlQuery = "SELECT IP,Name FROM Servers";
            // WHERE Time >= '" + (curTime - 10) +"'"
            dbcmd.CommandText = sqlQuery;
            reader = dbcmd.ExecuteReader();
            while (reader.Read())
            {
                if(!Servers.ContainsKey(reader.GetString(0)))
                {
                    Servers.Add(reader.GetString(0), reader.GetString(1));
                }
                 
                
            }
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            dbconn.Close();
            dbconn = null;
            
            foreach(KeyValuePair<string, ListEntry> old in servers)
            {
                if(!Servers.ContainsKey(old.Key))
                {
                    Destroy(old.Value.gameObject);
                    servers.Remove(old.Key);
                }
            }

            foreach(KeyValuePair<string, string> res in Servers)
            {
                if(!servers.ContainsKey(res.Key))
                {
                    servers.Add(res.Key, AddServer(res.Key, res.Value));
                }
            }

            yield return new WaitForSeconds(1);
        }
    }


    public void StartHost()
    {
        dbconn = (IDbConnection)new SqliteConnection(DbLoc);
        dbconn.Open(); //Open connection to the database.
        dbcmd = dbconn.CreateCommand();
        string sqlQuery = "INSERT INTO Servers(IP, Name) VALUES('" + Network.player.ipAddress + "', '" + ComputerStatics.Name + "''s Server')";
        dbcmd.CommandText = sqlQuery;
        dbcmd.ExecuteNonQuery();
        
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }

    private void OnApplicationQuit()
    {
        if (dbcmd != null)
        {
            dbcmd.Dispose();
            dbcmd = null;
        }
        
        if(dbconn != null)
        {
            dbconn.Close();
            dbconn = null;
        }
        if (reader != null)
        {
            reader.Close();
            reader = null;
        }
            
    }

}
