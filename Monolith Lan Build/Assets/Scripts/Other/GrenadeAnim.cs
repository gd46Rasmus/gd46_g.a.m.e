﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeAnim : MonoBehaviour {

    public Grenade grenade;

    AudioSource aSource;

    private void Start()
    {
        aSource = GetComponent<AudioSource>();
    }

    void Hide()
    {
        GetComponent<SpriteRenderer>().enabled = false;
    }

    void StartSound()
    {
        aSource.Play();
    }

    void Explode()
    {
        grenade.Cmd_Explodee();
    }

    void Delete()
    {
        grenade.Final();
    }
}
