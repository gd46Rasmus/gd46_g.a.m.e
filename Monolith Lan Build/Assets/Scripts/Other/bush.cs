﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bush : MonoBehaviour {

    AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponentInParent<Character>() != null)
        {
            audioSource.Play();
        }
    }
}
