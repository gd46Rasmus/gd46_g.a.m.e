﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerStatics : MonoBehaviour {

    static string playerName = null;
    public static string Name
    {
        get { return playerName; }
    }

    public static void SetName(string name)
    {
        if(playerName == null)
        {
            playerName = name;
        }
    }
}
