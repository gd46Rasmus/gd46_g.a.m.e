﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundLibrary : MonoBehaviour {


    public static AudioClip[] grunts;

    public AudioClip[] g1;

    public AudioClip shotGunSound;
    public static AudioClip shotGunSouds;

    public AudioClip[] steps;
    public static AudioClip[] sstep;

    public static AudioClip FLOn;
    public static AudioClip FLOff;

    public AudioClip[] FL;

    public static AudioClip Sniper;
    public AudioClip SniperSound;

    public AudioClip[] Swords;
    public static AudioClip[] SwordSounds;

    private void Start()
    {
        SwordSounds = Swords;
        Sniper = SniperSound;
        FLOn = FL[0];
        FLOff = FL[1];

        shotGunSouds = shotGunSound;
        grunts = g1;
        sstep = steps;
    }
}
