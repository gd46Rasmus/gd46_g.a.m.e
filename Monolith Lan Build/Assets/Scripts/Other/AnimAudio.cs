﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimAudio : MonoBehaviour {

    AudioSource source;

    private void Start()
    {
        source = GetComponent<AudioSource>();
    }

    void PlaySound()
    {

        source.clip = SoundLibrary.sstep[Random.Range(0, SoundLibrary.sstep.Length - 1)];
        source.Play();

    }

    void DisableSword()
    {
        GetComponentInParent<Melee>().StopAttack();
    }
    
}
