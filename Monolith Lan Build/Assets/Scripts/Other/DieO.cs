﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieO : MonoBehaviour {

	public void StartSound(AudioClip sound)
    {
        GetComponent<AudioSource>().clip = sound;
        GetComponent<AudioSource>().Play();

        StartCoroutine(Die());
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(10);

        Destroy(gameObject);
    }
}
