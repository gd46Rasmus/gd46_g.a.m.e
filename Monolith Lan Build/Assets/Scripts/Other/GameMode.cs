﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using Mono.Data.Sqlite;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class GameMode : NetworkBehaviour
{
    IDbConnection dbconn;
    IDbCommand dbcmd;

    //editor variables
    public float mapLength = 1;

    private string DbLoc = "URI=file:\\\\vfsstorage10\\GameDesign\\Students\\Shared_Student_Storage\\GD46\\Monolith\\Sqlite\\MonoServers.db";

    public Dictionary<PlayerController, uint> kills = new Dictionary<PlayerController, uint>();

    [SyncVar(hook = "gameStateUpdate")] bool gameIsActive = false;
    public static bool inGame = false;
    [SyncVar] int lastPlayer = 1;

    [HideInInspector]
    public bool endIsLoaded = false;

    Dictionary<PlayerController, int> playerIds = new Dictionary<PlayerController, int>();

    //to be implemented
    //level things

    //time start of game
    [SyncVar] private float startTime;
    private static float timeLeft;
    float diff;


    void gameStateUpdate(bool newState)
    {
        gameIsActive = newState;
        inGame = newState;

        
        
        foreach (PlayerController pc in FindObjectsOfType<PlayerController>())
        {
            kills.Add(pc, 0);
            if (isServer)
            {
                pc.Rpc_MSG();
                pc.GameStart();
            }
        }

        if(isServer)
        {
            System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
            int curTime = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;

            
            dbconn = (IDbConnection)new SqliteConnection(DbLoc);
            dbconn.Open(); //Open connection to the database.
            dbcmd = dbconn.CreateCommand();
            string sqlExec = "DELETE FROM Servers WHERE IP = '" + Network.player.ipAddress + "'";

            dbcmd.CommandText = sqlExec;
            dbcmd.ExecuteNonQuery();
            
            dbcmd.Dispose();
            dbcmd = null;
            dbconn.Close();
            dbconn = null;
        }
        
    }
    

    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);

        if(gameIsActive)
        {
            inGame = true;
            Cmd_GetTimeDiff();
        }
    }

    public static float GetTimeLeft()
    {
        return timeLeft;
    }

    void Update()
    {

        if(!inGame && isServer)
        {
            return;
        }

        if (!inGame || endIsLoaded) return;

        if ((Time.time + diff) >= startTime + (mapLength * 60) && !endIsLoaded)
        {
            GetComponent<AudioSource>().Stop();
            SceneManager.LoadScene("EndScreen");
        }
        timeLeft = (startTime + (mapLength * 60)) - (Time.time + diff);

        

        if (!isServer) return;
        
        foreach(KeyValuePair<PlayerController, uint> v in kills)
        {
            Rpc_UpdateScores(v.Key.gameObject, v.Value);
        }

    }

    [ClientRpc]
    void Rpc_UpdateScores(GameObject p, uint k)
    {
        kills[p.GetComponent<PlayerController>()] = k;
    }

    [Command]
    public void Cmd_GetPlayerIndex(GameObject pc)
    {
        PlayerController pcc = pc.GetComponent<PlayerController>();
        if (lastPlayer >= 4)
        {
            List<int> found = new List<int> { 1, 2, 3, 4 };
            foreach (KeyValuePair<PlayerController, int> res in playerIds)
            {
                found.Remove(res.Value);
            }

            pcc.playerN = found[0];
            playerIds.Add(pcc, found[0]);

        }
        else
        {
            playerIds.Add(pcc, lastPlayer);
            pcc.playerN = lastPlayer++;
        }
    }

    public void ForceStart()
    {
        Cmd_StartGame();
    }

    [Command]
    void Cmd_StartGame()
    {
        startTime = Time.time;
        timeLeft = (mapLength * 60);

        Rpc_TimeDiff(Time.time);

        gameIsActive = true;
    }

    [ClientRpc]
    void Rpc_TimeDiff(float time)
    {
        diff = time - Time.time;
    }

    [Command]
    void Cmd_GetTimeDiff()
    {
        Rpc_TimeDiff(Time.time);
    }

    public int GetPlayers()
    {
        return lastPlayer;
    }

    IEnumerator Broadcast()
    {
        while(!inGame)
        {
            System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
            int curTime = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;

            dbconn = (IDbConnection)new SqliteConnection(DbLoc);
            dbconn.Open(); //Open connection to the database.
            dbcmd = dbconn.CreateCommand();
            string sqlExec = "UPDATE Servers SET Time = '" + (curTime) + "' WHERE Name = '" + ComputerStatics.Name + "''s Server'";

            dbcmd.CommandText = sqlExec;
            dbcmd.ExecuteNonQuery();
            
            dbcmd.Dispose();
            dbcmd = null;
            dbconn.Close();
            dbconn = null;

            yield return new WaitForSeconds(3);
        }
        
    }

    private void OnApplicationQuit()
    {
        if (dbcmd != null)
        {
            dbcmd.Dispose();
            dbcmd = null;
        }

        if (dbconn != null)
        {
            dbconn.Close();
            dbconn = null;
        }
    }
}
