﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour {

    float searchRadius = 500;

	public int SpawnValue()
    {
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(transform.position, searchRadius);

        int hitChars = 0;

        foreach(Collider2D curr in hitColliders)
        {
            if(curr.gameObject.GetComponent<Character>() != null || curr.gameObject.GetComponentInChildren<Character>() != null || curr.gameObject.GetComponentInParent<Character>() != null)
            {
                hitChars++;
            }
        }

        return 10 - hitChars;
    }
}
