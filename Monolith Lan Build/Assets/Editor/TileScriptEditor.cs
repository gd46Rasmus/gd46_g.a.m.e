﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TileScript))]
[CanEditMultipleObjects]
public class TileScriptEditor : Editor
{

    public override void OnInspectorGUI()
    {
        TileScript tile = (TileScript)target;

        GUILayout.Label("--- Snapping ---");
        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();

        tile.useSnap = GUILayout.Toggle(tile.useSnap, GUIContent.none, GUILayout.Width(20));

        GUILayout.Label("Use Snapping?");
        GUILayout.EndHorizontal();

        if (tile.useSnap)
        {
            GUILayout.BeginHorizontal();
            tile.useCustomSnap = GUILayout.Toggle(tile.useCustomSnap, GUIContent.none, GUILayout.Width(20));

            GUILayout.Label("Use Custom Snapping?");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            tile.UniformSnap = GUILayout.Toggle(tile.UniformSnap, GUIContent.none, GUILayout.Width(20));

            GUILayout.Label("Uniform snapping?");
            GUILayout.EndHorizontal();

            if (tile.useCustomSnap)
            {
                if (tile.UniformSnap)
                {
                    GUILayout.BeginHorizontal();

                    GUILayout.Label("Snap size:");
                    tile.snapSize = EditorGUILayout.FloatField(tile.snapSize);

                    GUILayout.EndHorizontal();


                }
                else
                {
                    GUILayout.BeginHorizontal();

                    GUILayout.Label("Snap size");

                    GUILayout.Label("X:");
                    tile.snapX = EditorGUILayout.FloatField(tile.snapX);

                    GUILayout.Label("Y:");
                    tile.snapY = EditorGUILayout.FloatField(tile.snapY);


                    GUILayout.EndHorizontal();
                }
            }
            else
            {
                if (tile.UniformSnap)
                {

                    GUILayout.BeginHorizontal();

                    GUILayout.Label("Snap size: " + tile.snapSize);

                    GUILayout.EndHorizontal();

                }
                else
                {
                    GUILayout.BeginHorizontal();

                    GUILayout.Label("Snap size X: " + tile.snapX + " Y: " + tile.snapY);

                    GUILayout.EndHorizontal();
                }
            }

            if (GUILayout.Button("Recalculate"))
            {
                tile.Recalculate();
            }
        }



        GUILayout.EndVertical();

        GUILayout.Label("--- Rotation ---");

        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();


        GUILayout.Label("Rotation size");
        tile.rotationSnapSize = EditorGUILayout.FloatField(tile.rotationSnapSize);

        GUILayout.EndHorizontal();
        GUILayout.EndVertical();


        GUILayout.Label("--- Controlls ---");




        GUILayout.BeginVertical();


        GUILayout.BeginHorizontal();
        tile.copy = GUILayout.Toggle(tile.copy, GUIContent.none, GUILayout.Width(20));

        GUILayout.Label("Move a Copy?");
        GUILayout.EndHorizontal();


        GUILayout.BeginHorizontal();
        TileScript c;
        if (GUILayout.Button("\u21BB", GUILayout.Height(40), GUILayout.Width(40)))
        {
            tile.Rotate(true);
        }

        if (GUILayout.Button("\u2191", GUILayout.Height(40), GUILayout.Width(40)))
        {
            if((c = tile.Move(0, 1)) != null)
            {
                Selection.activeTransform = c.transform;
            }
        }

        if (GUILayout.Button("\u21BA", GUILayout.Height(40), GUILayout.Width(40)))
        {
            tile.Rotate(false);
        }


        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("\u2190", GUILayout.Height(40), GUILayout.Width(40)))
        {
            if ((c = tile.Move(-1,0)) != null)
            {
                Selection.activeTransform = c.transform;
            }
        }

        if (GUILayout.Button("\u2193", GUILayout.Height(40), GUILayout.Width(40)))
        {
            if ((c = tile.Move(0, -1)) != null)
            {
                Selection.activeTransform = c.transform;
            }
        }

        if (GUILayout.Button("\u2192", GUILayout.Height(40), GUILayout.Width(40)))
        {
            if ((c = tile.Move(1, 0)) != null)
            {
                Selection.activeTransform = c.transform;
            }
        }

        GUILayout.EndHorizontal();





        GUILayout.EndVertical();
    }
}


