﻿
/// <summary>
///     By: Sebastian Lague
///     
///     Source: https://github.com/SebLague/Field-of-View
/// </summary>

Shader "Custom/LightObjectShader" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
	_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_InRange("Inside Range", Range(0,2)) = 0
		_TranRange("Transition Range", Range(0,2)) = 0
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" "Queue" = "Geometry-100" }
		ColorMask 0
		ZWrite off
		LOD 200

		Stencil{
		Ref 1
		Pass replace
	}

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
#pragma target 3.0

		sampler2D _MainTex;

	struct Input {
		float4 vertex   : POSITION;
		float2 uv_MainTex;
	};

	half _Glossiness;
	half _Metallic;
	fixed4 _Color;
	float _InRange;
	float _TranRange;

	void surf(Input IN, inout SurfaceOutputStandard o) {
		// Albedo comes from a texture tinted by color
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		float2 d=IN.vertex;
		float dist = distance(float2(0, 0), d);
		float a = saturate(((_InRange + _TranRange) - dist) / _TranRange);
		o.Albedo = c.rgb;
		// Metallic and smoothness come from slider variables
		o.Metallic = _Metallic;
		o.Smoothness = _Glossiness;
		o.Alpha = c.a;
	}
	ENDCG
	}
		FallBack "Diffuse"
}