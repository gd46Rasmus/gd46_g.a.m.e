﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestShadowWalker : MonoBehaviour
{
	private Animator aShadowWalker;

	void Awake ()
	{
		aShadowWalker = GetComponent <Animator> ();
	}

	void Update ()
	{
		if (Input.GetKey (KeyCode.W)) // PRESS W TO RUN.
		{
			aShadowWalker.SetBool ("bIsRunning", true);
		}
		else
		{
			aShadowWalker.SetBool ("bIsRunning", false);
		}

		if (Input.GetKey (KeyCode.A)) // PRESS A TO SWING.
		{
			aShadowWalker.SetTrigger ("tIsSwinging");
		}

		if (Input.GetKey (KeyCode.S)) // PRESS S TO THRUST.
		{
			aShadowWalker.SetTrigger ("tIsThrusting");
		}
	}
}