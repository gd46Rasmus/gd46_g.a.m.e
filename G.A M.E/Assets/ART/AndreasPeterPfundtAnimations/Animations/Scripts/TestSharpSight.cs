﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSharpSight : MonoBehaviour
{
	private Animator aSharpSight;

	void Awake ()
	{
		aSharpSight = GetComponent <Animator> ();
	}

	void Update ()
	{
		if (Input.GetKey (KeyCode.W)) // PRESS W TO RUN.
		{
			aSharpSight.SetBool ("bIsRunning", true);
		}
		else
		{
			aSharpSight.SetBool ("bIsRunning", false);
		}

		if (Input.GetKeyDown (KeyCode.A)) // PRESS A TO INITIATE READY STANCE AND AIM.
		{
			aSharpSight.SetTrigger ("tIsReadying");
			aSharpSight.SetBool ("bIsAiming", true); // AIMING IS BROKEN AT THE MOMENT.
		}

		if (Input.GetKey (KeyCode.S)) // PRESS S TO CANCEL THE AIM.
		{
			aSharpSight.SetBool ("bIsAiming", false);
		}

		if (Input.GetKey (KeyCode.D)) // PRESS D TO TAKE THE SHOT.
		{
			aSharpSight.SetTrigger ("tIsSniping"); // SNIPING IS BROKEN AT THE MOMENT.
		}
	}
}