﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestEngager : MonoBehaviour
{
	public GameObject gOLandMine;

	private Animator aEngager;

	void Awake ()
	{
		aEngager = GetComponent <Animator> ();
	}

	void Update ()
	{
		if (Input.GetKey (KeyCode.W)) // PRESS W TO RUN.
		{
			aEngager.SetBool ("bIsRunning", true);
		}
		else
		{
			aEngager.SetBool ("bIsRunning", false);
		}

		if (Input.GetKey (KeyCode.A)) // PRESS A TO THROW.
		{
			aEngager.SetTrigger ("tIsThrowing");

			gOLandMine.SetActive (false);
		}

		if (Input.GetKey (KeyCode.S)) // PRESS S TO DETONATE.
		{
			aEngager.SetTrigger ("tIsDetonating");

			gOLandMine.SetActive (true);
		}
	}
}