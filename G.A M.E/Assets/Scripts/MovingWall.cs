﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MovingWall : MonoBehaviour {

    public float time;
    public Vector3 targetLocation;
    Tweener moveTween;

	// Use this for initialization
	void Start () {

        targetLocation = transform.position + targetLocation;

        moveTween = transform.DOMove(targetLocation, time);
        moveTween.SetEase(Ease.InOutSine);
        moveTween.SetLoops(-1, LoopType.Yoyo);
    }
}
