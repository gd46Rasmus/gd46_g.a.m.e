﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public float damage = 10;
    public float speed = 20;
    public float lifeSpan = .9f;

    public Character gunner;

    // Use this for initialization
    void Start() {
        StartCoroutine(Die());
        GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Cos(transform.rotation.eulerAngles.z * Mathf.Deg2Rad) * speed, Mathf.Sin(transform.rotation.eulerAngles.z * Mathf.Deg2Rad) * speed);
    }
    // Update is called once per frame
    void Update() {

    }

    void OnTriggerEnter2D(Collider2D otherActor)
    {
        Character hitActor = otherActor.gameObject.GetComponentInParent<Character>();
        
        
        if(hitActor != null)
        {
            hitActor.TakeDamage(damage, gunner);
        }
        Destroy(this.gameObject);
    }

    protected IEnumerator Die()
    {
        yield return new WaitForSeconds(lifeSpan);
        Destroy(gameObject);
    }
}
