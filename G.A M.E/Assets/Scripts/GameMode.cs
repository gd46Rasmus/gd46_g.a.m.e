﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMode : MonoBehaviour {

    //editor variables
    public float mapLength = 1;

    //player stats
    public static int[] playerKills = { 0, 0, 0, 0};
    public static int[] playerDeaths = { 0, 0, 0, 0};

    //to be implemented
    //level things
    private PlayerSpawn[] spawns;

    //time start of game
    private float startTime;
    private static float timeLeft;

	void Start () {
        timeLeft = (mapLength * 60);
        spawns = FindObjectsOfType<PlayerSpawn>();
        startTime = Time.time;
    }

	public static float GetTimeLeft()
    {
        return timeLeft;
    }

	void Update () {
        if (Time.time >= startTime + (mapLength * 60))
        {
            SceneManager.LoadScene("EndScreen");
        }
        timeLeft = (startTime + (mapLength * 60)) - Time.time;

    }
}
