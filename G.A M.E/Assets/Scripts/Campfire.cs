﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Campfire : MonoBehaviour {
    // Use this for initialization
    public static List<Character> charsInRange = new List<Character>();
    public LayerMask layers;

    void Start () {
        GetComponent<CircleCollider2D>().radius = GetComponent<FieldOfView>().viewRadius;
        StartCoroutine(stopUpdates());
	}
	
    //function to disable updates of light mesh after first frame
	IEnumerator stopUpdates()
    {
        yield return new WaitForEndOfFrame();

        GetComponent<FieldOfView>().doUpdates = false;
    }

    void OnTriggerEnter2D(Collider2D otherPawn)
    {
        if(otherPawn.GetComponentInParent<Character>() != null)
        {
            //if the other actor is a character add to list of characters in campfire range
            charsInRange.Add(otherPawn.GetComponentInParent<Character>());
        }
    }

    void OnTriggerExit2D(Collider2D otherPawn)
    {
        if (otherPawn.GetComponentInParent<Character>() != null)
        {
            //remove character when it leaves
            Character pawn = otherPawn.GetComponentInParent<Character>();
            for (int i = 0; i < charsInRange.Count; i++)
            {
                if(pawn.GetInstanceID() == charsInRange[i].GetInstanceID())
                {
                    charsInRange.RemoveAt(i);
                    return;
                }
            }
        }
    }
}
