﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public class Character : MonoBehaviour {

    //stats
    public float speed;
    public float maxHealth = 100;
    private float health;

    public float cameraAheadDist = 28;

    //References
    public MeshRenderer fow;
    public MeshRenderer area;

    public GameObject pulseSphere;

    public GameObject meleeObject;
    public GameObject projectileSpawner;
    protected PlayerController owner;
    protected Rigidbody2D rigid;
    public GameObject sprite;
    protected FieldOfView fieldOfView;

    public GameObject cameraPoint;

    GameObject swingAnchor;

    //to be removed later

    public GameObject Indicator;

    //sprint variables

    public float sprintMax;
    public float sprintRegen;
    private float sprintCurrent = 0;

    private float respawnInvinciblity = 3;
    private float dmgTime;
    //stuffs
    public Color charColor;
    protected int playerIndex;
    
    //status
    private bool isActive = false;

    // Use this for initialization
    void Start ()
    {
        dmgTime = Time.time + respawnInvinciblity;
        health = maxHealth;
        fieldOfView = GetComponent<FieldOfView>();
        rigid = GetComponent<Rigidbody2D>();
        //fieldOfView.doUpdates = false;
    }

    void Update()
    {
        sprintUpdate();
    }

    public PlayerController GetPlayerController()
    {
        return owner;
    }

    public int GetPlayerIndex()
    {
        return playerIndex;
    }

    public void SetPlayerController(PlayerController pc)
    {
        owner = pc;
    }

    //used for environmental hazards
    public void TakeDamage(float damage)
    {

        health -= damage;

        if (health <= 0)
        {
            Destroy(this.gameObject);
            GameMode.playerDeaths[playerIndex - 1]++;
        }
        
    }

    //for player attacks
    public void TakeDamage(float damage, Character killer)
    {
        if(Time.time >= dmgTime)
        {
            health -= damage;
        }

        if (health <= 0)
        {
            //diable haptics
            GamePad.SetVibration(owner.getPlayerIndex(), 0, 0);
            //add kills and death to correct player
            GameMode.playerDeaths[playerIndex - 1]++;
            GameMode.playerKills[killer.playerIndex - 1]++;
            Destroy(this.gameObject);
        }

    }

    public virtual void Move(float x, float y)
    {
        Vector2 newPosition = new Vector2(transform.position.x + (x * speed), transform.position.y + (y * speed));
        rigid.MovePosition(newPosition);
    }
    
    public virtual void Rotate(float x, float y)
    {
        //float rotation = sprite.transform.eulerAngles.z;
        float rotation = Mathf.Atan2(-y, x) * Mathf.Rad2Deg;
        
        transform.eulerAngles = new Vector3(0, 0, 0);
        sprite.transform.eulerAngles = new Vector3(0, 0, -rotation);
        
        //give the field of view script the angle it need
        fieldOfView.currentZAngle = -rotation;

        
    }
    
    //move the anchor for camera
    public void CameraPoint(float x, float y)
    {
        cameraPoint.transform.localPosition = Vector3.Lerp(cameraPoint.transform.localPosition, new Vector3(x * cameraAheadDist, y * cameraAheadDist, 0), Time.deltaTime);
    }

    //attack function, overriden in children
    public virtual void Attack()
    {
        
    }

    //turn on/off light
    public void SwapLightStatus()
    {
        //stop updating light mesh and disable it
        fow.enabled = !fow.enabled;
        fieldOfView.doUpdates = fow.enabled;
    }

    //set layer to make sure the correct cameras render objects
    public void SetPlayerLayer(int player)
    {
        Indicator.layer = 8 + player;
        area.gameObject.layer = 8 + player;
        
        sprite.GetComponent<SpriteRenderer>().color = charColor;
        playerIndex = player;

    }

    //function for special move, overriden in all children using it
    public virtual void SpecialMove()
    {

    }


    //to be removed
    //recharge sprint when not sprinting
    protected void sprintUpdate()
    {
        sprintCurrent += sprintRegen * Time.deltaTime;

        if(sprintCurrent > sprintMax)
        {
            sprintCurrent = sprintMax;
        }
    }

    //to be removed
    //decrease sprint when sprinting
    public void SprintMeter()
    {
        sprintCurrent -= Time.deltaTime;
        
    }


    //to be removed
    public bool CanSprint()
    {
        if (sprintCurrent > 0)
        {
            return true;
        }

        return false;
    }

    //haptic time
    public IEnumerator doHaptic(float time)
    {
        yield return new WaitForSeconds(time);
        GamePad.SetVibration(owner.getPlayerIndex(), 0, 0);
    }
}
