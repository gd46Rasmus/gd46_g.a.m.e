﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public class Range : Character {
    
    public float reloadSpeed = .5f;
    public float snipeReloadSpeed = 2;
    private float nextShot = 0;
    public Projectile NormalProjectile;
    public Projectile SniperProjectile;

    private bool inSnipeMode = false;

    private float fowAngle;
    private float fowDist;

    public float snipeAngle = 45;
    public float snipeDist = 600;
    public float snipeZoom = 100;
    public float snipeMoveSpeed = .2f;
    private float normalSpeed;

    public float snipeCameraAhead = 40;

    public override void Attack()
    {
        
        if (nextShot <= Time.time)
        {
            Projectile spawnedProjectile;
            if (!inSnipeMode)
            {
                GamePad.SetVibration(owner.getPlayerIndex(), 1, 1);
                StartCoroutine(doHaptic(.1f));
                spawnedProjectile = Instantiate(NormalProjectile, projectileSpawner.transform.position, sprite.transform.rotation);
                nextShot = Time.time + reloadSpeed;
            }
            else
            {
                GamePad.SetVibration(owner.getPlayerIndex(), 1, 1);
                StartCoroutine(doHaptic(.3f));
                SpecialMove();
                spawnedProjectile = Instantiate(SniperProjectile, projectileSpawner.transform.position, sprite.transform.rotation);
                nextShot = Time.time + snipeReloadSpeed;
            }

            spawnedProjectile.gunner = this;
            
        }
    }

    public override void SpecialMove()
    {
        if(inSnipeMode)
        {
            inSnipeMode = false;
            fieldOfView.viewAngle = fowAngle;
            fieldOfView.viewRadius = fowDist;
            owner.playerCamera.GetComponent<Camera>().orthographicSize = owner.playerCamera.GetComponent<CameraController>().cameraZoom;
            speed = normalSpeed;
            cameraAheadDist -= snipeCameraAhead;
        }
        else
        {

            //set varibles to reset to old fow
            inSnipeMode = true;
            fowAngle = fieldOfView.viewAngle;
            fowDist = fieldOfView.viewRadius;

            fieldOfView.viewAngle = snipeAngle;
            fieldOfView.viewRadius = fowDist + snipeDist;
            normalSpeed = speed;
            speed = snipeMoveSpeed;
            cameraAheadDist += snipeCameraAhead;

            owner.playerCamera.GetComponent<Camera>().orthographicSize = owner.playerCamera.GetComponent<CameraController>().cameraZoom + snipeZoom;
        }
    }

    
}
