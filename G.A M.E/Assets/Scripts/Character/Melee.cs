﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee : Character {

    public float meleeSpeed = 1.4f;
    float nextMelee = 0;

    bool meleeIsActive = false;
    bool isSpinning = false;
    public float spinDuration = 3;
    public float spinSpeed = 300;

    private float nextSpin = 0;
    public float spinCooldown = 1;

    public float swingAngle = 90;
    public float swingSpeed = 180;

    private Tweener spinTween;

    void Update()
    {
        if(meleeIsActive && meleeObject.transform.localEulerAngles.z <  180 + (swingAngle / 2))
        {
            meleeObject.transform.localEulerAngles = new Vector3(0, 0, meleeObject.transform.localEulerAngles.z + (swingSpeed * Time.deltaTime));
        }
        else if(isSpinning)
        {
            meleeObject.transform.eulerAngles = new Vector3(0, 0, meleeObject.transform.eulerAngles.z + ((360/spinDuration) * Time.deltaTime));
        }
        else
        {
            meleeObject.SetActive(false);
            meleeIsActive = false;
        }

        sprintUpdate();
    }

    //do a normal swing
    public override void Attack()
    {
        if(nextMelee <= Time.time && !meleeIsActive && !isSpinning)
        {
            meleeIsActive = true;
            meleeObject.SetActive(true);
            meleeObject.transform.localEulerAngles = new Vector3(0, 0, 180 - (swingAngle / 2));
            nextMelee = Time.time + meleeSpeed;
        }
        
    }

    //special move, spin attack
    public override void SpecialMove()
    {
        if(nextSpin <= Time.time)
        {
            //reset melee rotaion and enable weapon, set bools
            meleeObject.transform.localEulerAngles = new Vector3(0, 0, 180);
            meleeIsActive = false;
            meleeObject.SetActive(true);
            isSpinning = true;
            StartCoroutine(spin());

            nextMelee = Time.time + spinDuration + spinCooldown;
            nextSpin = Time.time + spinDuration + spinCooldown;
        }
    }

    //spin attack
    IEnumerator spin()
    {
        yield return new WaitForSeconds(spinDuration);
        isSpinning = false;
    }

}
