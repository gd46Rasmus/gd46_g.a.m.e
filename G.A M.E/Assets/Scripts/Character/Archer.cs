﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public class Archer : Character {

    public Arrow arrowPrefab;

    public float drawCooldown = .2f;
    public float maxDrawTime = 2;
    public float currentDrawDuration = 0;

    public float drawMultiplier = 1;

    bool drawOnCooldown = false;
    bool hasDrawn = false;
    float lastRecordedDraw = 0;

    PlayerIndex controller;
    GamePadState state;
    GamePadState prevState;
    bool controllerIsSet = false;

    void Fire()
    {

        float currentDrawDurationMath = currentDrawDuration;

        if (currentDrawDuration > 3)
        {
            currentDrawDurationMath = 3;
        }
        else if (currentDrawDuration < .5f)
        {
            MissFire();
            return;
        }

        Arrow spawnedProjectile = Instantiate(arrowPrefab, projectileSpawner.transform.position, sprite.transform.rotation);
        //spawnedProjectile.speed *= (currentDrawDuration / maxDrawTime) * drawMultiplier;
        //spawnedProjectile.speed *= (2 - (Mathf.Log10(currentDrawDuration - .02f) - .3f)) * drawMultiplier;
        //0.4e0.8047
        

        spawnedProjectile.speed *=  (.4f * Mathf.Exp(.8047f * currentDrawDurationMath));
       
        spawnedProjectile.Spawn();
        spawnedProjectile.gunner = this;
        StartCoroutine(DrawCooldown());
    }

    //do cooldown without shooting
    void MissFire()
    {
        StartCoroutine(DrawCooldown());
    }

    //tracking of controller specific to archer
    void LateUpdate()
    {
        if (!controllerIsSet)
        {
            controller = owner.getPlayerIndex();

        }
        state = GamePad.GetState(controller);

        if (state.Triggers.Right < .4f && currentDrawDuration > 0)
        {
            if(currentDrawDuration > 3.5f)
            {
                MissFire();
            }
            else
            {
                Fire();
            }
        }
    }

    //charge
    public override void Attack()
    {
        currentDrawDuration += Time.deltaTime;

        if(currentDrawDuration > maxDrawTime)
        {
            MissFire();
        }
    }

    IEnumerator DrawCooldown()
    {
        currentDrawDuration = 0;
        drawOnCooldown = true;
        yield return new WaitForSeconds(drawCooldown);
        drawOnCooldown = false;
    }

    public override void SpecialMove()
    {
    }
}
