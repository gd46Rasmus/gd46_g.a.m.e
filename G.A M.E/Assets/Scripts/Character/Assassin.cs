﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Assassin : Character {

    bool isNightVision = false;
    bool isDashLocked = false;

    public float dashCooldown = 4;
    float nextDash = 0;

    public float dashRange = 100;
    public float dashSpeed = 1;
    public float normalVision = 90;
    public float nightVisionRange = 20;
    private float dashTimer = -1;
    private Vector3 dashVelocity;

    public AudioSource nvAudioSource;
    
    public AudioClip nightVisionOn;
    public AudioClip nightVisionOff;

    void Update()
    {
        if(isNightVision && fieldOfView.viewRadius != nightVisionRange)
        {
            fieldOfView.viewRadius = 30;
        }
        else if(!isNightVision)
        {
            fieldOfView.viewRadius = 60;
        }

        if(Time.time < dashTimer)
        {
            rigid.velocity = dashVelocity;
        }
        else
        {
            meleeObject.SetActive(false);
            isDashLocked = false;
        }

        sprintUpdate();
    }


    //start a normal attack
    public override void Attack()
    {
        if(!isDashLocked)
        {
            if(nextDash <= Time.time)
            {
                Vector3 target = new Vector3(Mathf.Cos(sprite.transform.eulerAngles.z * Mathf.Deg2Rad), Mathf.Sin(sprite.transform.eulerAngles.z * Mathf.Deg2Rad), 0);

                isDashLocked = true;
                meleeObject.SetActive(true);

                meleeObject.transform.localEulerAngles = new Vector3(0, 0, 180);
                dashVelocity = target * dashSpeed;
                dashTimer = Time.time + dashRange;

                nextDash = Time.time + dashCooldown;
            }
        }
        
    }
    //night vision toggle
    public override void SpecialMove()
    {
        if (isNightVision)
        {
            nvAudioSource.Pause();
            isNightVision = false;
            fow.gameObject.layer = 13;
        }
        else
        {
            nvAudioSource.Play();
            isNightVision = true;
            fow.gameObject.layer = 8 + playerIndex;
        }
    }
    //special move and rotate for assassin
    public override void Move(float x, float y)
    {
        if(!isDashLocked)
        {
            base.Move(x, y);
        }
    }

    public override void Rotate(float x, float y)
    {
        if(!isDashLocked)
        {
            base.Rotate(x, y);
        }
    }
}
