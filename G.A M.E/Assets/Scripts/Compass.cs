﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Compass : MonoBehaviour {

    public Sprite Melee;
    public Sprite Ranger;
    public Sprite Assassin;

    SpriteRenderer renderComponent;

	// Use this for initialization
	void Start () {
        renderComponent = GetComponent<SpriteRenderer>();
        
    }
	
	public void SetRotation(Character target)
    {
        if(target == null)
        {
            Campfire.charsInRange.Remove(target);
            return;
        }

        if ((target.transform.position - transform.position).magnitude > 1)
        {
            SetClass(target);
            float x = target.transform.position.x - transform.position.x;
            float y = target.transform.position.y - transform.position.y;
            transform.eulerAngles = new Vector3(0, 0, (Mathf.Atan2(y, x) * Mathf.Rad2Deg) - 90);
        }
    }

    public void SetClass(Character target)
    {
        if(renderComponent != null)
        {
            if (target.gameObject.GetComponent<Melee>() != null)
            {
                renderComponent.sprite = Melee;
            }
            else if (target.gameObject.GetComponent<Range>() != null)
            {
                renderComponent.sprite = Ranger;
            }
            else
            {
                renderComponent.sprite = Assassin;
            }
        }   
    }

    public void SetLayer(LayerMask layer)
    {
        gameObject.layer = layer;
    }
    
}
