﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    private Character target;
    public float cameraZoom = 280;

	// Update is called once per frame
	void Update () {
        
        if (target != null)
        {
            
            
            Vector3 position = target.cameraPoint.transform.position;

            position.z -= 60;

            transform.position = position;
        }
	}

    public void NewTarget(Character target)
    {
        this.target = target;
        GetComponent<Camera>().orthographicSize = cameraZoom;
    }

    public void SetPlayer(int player, int playerCount)
    {
        Camera cam = GetComponent<Camera>();

        

        Vector2 location = new Vector2();
        Vector2 size = new Vector2(0.4975f, 0.495f);

        if (player == 1)
        {
            location = new Vector2(0, 0.505f);
        }
        else if(player == 2)
        {
            location = new Vector2(0.5025f, 0.505f);
        }
        else if(player == 3)
        {
            location = new Vector2(0f, 0);
        }
        else if(player == 4)
        {
            location = new Vector2(0.5025f, 0);
        }
        

        cam.rect = new Rect(location, size);

    }
}
