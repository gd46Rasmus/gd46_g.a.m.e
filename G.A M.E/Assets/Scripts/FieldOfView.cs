﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///     By: Sebastian Lague
///     Edited by: Rasmus Jakobsson    
/// 
/// 
///     Source: https://github.com/SebLague/Field-of-View
/// </summary>

public class FieldOfView : MonoBehaviour
{



    public float viewRadius;
    [Range(0, 360)]
    public float viewAngle;

    public float currentZAngle;

    public LayerMask obstacleMask;

    public float meshResolution;
    public int edgeResolveIterations;
    public float edgeDstThreshold;

    public float maskCutawayDst = .1f;

    public bool doUpdates = true;

    public MeshFilter viewMeshFilter;
    public MeshFilter colorMeshFilter;

    Mesh colorMesh;
    Mesh viewMesh;

    void Start()
    {
        viewMesh = new Mesh();
        viewMesh.name = "View Mesh";
        viewMeshFilter.mesh = viewMesh;

        if(colorMeshFilter != null)
        {
            colorMesh = new Mesh();
            colorMesh.name = "Color Mesh";
            colorMeshFilter.mesh = colorMesh;
        }
    }

    void LateUpdate()
    {
        if (doUpdates)
        {
            DrawFieldOfView();
        }
    }

    void DrawFieldOfView()
    {
        int stepCount = Mathf.RoundToInt(viewAngle * meshResolution);
        float stepAngleSize = viewAngle / stepCount;
        List<Vector2> viewPoints = new List<Vector2>();
        List<float> viewAngles = new List<float>();
        ViewCastInfo oldViewCast = new ViewCastInfo();
        for (int i = 0; i <= stepCount; i++)
        {
            float angle = currentZAngle - viewAngle / 2 + stepAngleSize * i;
            
            ViewCastInfo newViewCast = ViewCast(angle);

            if (i > 0)
            {
                bool edgeDstThresholdExceeded = Mathf.Abs(oldViewCast.dst - newViewCast.dst) > edgeDstThreshold;
                if (oldViewCast.hit != newViewCast.hit || (oldViewCast.hit && newViewCast.hit && edgeDstThresholdExceeded))
                {
                    EdgeInfo edge = FindEdge(oldViewCast, newViewCast);
                    if (edge.pointA != Vector2.zero)
                    {
                        viewPoints.Add(edge.pointA);
                        viewAngles.Add(edge.angleA);
                    }
                    if (edge.pointB != Vector2.zero)
                    {
                        viewPoints.Add(edge.pointB);
                        viewAngles.Add(edge.angleB);
                    }
                }

            }


            viewPoints.Add(newViewCast.point);
            viewAngles.Add(newViewCast.angle);
            oldViewCast = newViewCast;
        }
        

        int vertexCount = viewPoints.Count + 1;
        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];
        vertices[0] = Vector3.zero;

        Vector2[] uvs = new Vector2[viewPoints.Count + 1];


        for (int i = 0; i < vertexCount - 1; i++)
        {

            Vector2 dir = DirFromAngle(viewAngles[i], true);
            Vector3 forward = new Vector3(dir.x, dir.y);

            //Vector3 v3 = new Vector3(viewPoints[i].x - transform.position.x, viewPoints[i].y - transform.position.y, 0);
            Vector3 v3 = transform.InverseTransformPoint(viewPoints[i]) + (forward * maskCutawayDst);
            v3 = Vector3.ClampMagnitude(v3, viewRadius);
            vertices[i + 1] = v3;

            float u = (v3.magnitude / viewRadius);

            uvs[i] = new Vector2(u, 1);

            if (i < vertexCount - 2)
            {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }
        }

        viewMesh.Clear();
        

        viewMesh.vertices = vertices;
        viewMesh.triangles = triangles;
        viewMesh.RecalculateNormals();

        if(colorMesh != null)
        {


            uvs[0] = new Vector2(0, 1);

            colorMesh.Clear();

            colorMesh.vertices = vertices;
            colorMesh.triangles = triangles;
            colorMesh.RecalculateNormals();
            colorMesh.uv = uvs;
        }
    }

    EdgeInfo FindEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast)
    {
        float minAngle = minViewCast.angle;
        float maxAngle = maxViewCast.angle;
        Vector2 minPoint = Vector2.zero;
        Vector2 maxPoint = Vector2.zero;

        for (int i = 0; i < edgeResolveIterations; i++)
        {
            float angle = (minAngle + maxAngle) / 2;
            ViewCastInfo newViewCast = ViewCast(angle);

            bool edgeDstThresholdExceeded = Mathf.Abs(minViewCast.dst - newViewCast.dst) > edgeDstThreshold;
            if (newViewCast.hit == minViewCast.hit && !edgeDstThresholdExceeded)
            {
                minAngle = angle;
                minPoint = newViewCast.point;
            }
            else
            {
                maxAngle = angle;
                maxPoint = newViewCast.point;
            }
        }

        return new EdgeInfo(minPoint, maxPoint, minAngle, maxAngle);
    }


    ViewCastInfo ViewCast(float globalAngle)
    {
        Vector3 dir = DirFromAngle(globalAngle, true);
        RaycastHit2D hit;

        

        if (hit = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), dir, viewRadius, obstacleMask))
        {
            return new ViewCastInfo(true, hit.point, hit.distance, globalAngle);
        }
        else
        {
            return new ViewCastInfo(false, transform.position + dir * viewRadius, viewRadius, globalAngle);
        }
    }

    public Vector2 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.z;
        }
        return new Vector2(Mathf.Cos(angleInDegrees * Mathf.Deg2Rad), Mathf.Sin(angleInDegrees * Mathf.Deg2Rad));
    }
    


    public struct ViewCastInfo
    {
        public bool hit;
        public Vector2 point;
        public float dst;
        public float angle;

        public ViewCastInfo(bool _hit, Vector2 _point, float _dst, float _angle)
        {
            hit = _hit;
            point = _point;
            dst = _dst;
            angle = _angle;
        }
    }

    public struct EdgeInfo
    {
        public Vector2 pointA;
        public Vector2 pointB;
        public float angleA;
        public float angleB;

        public EdgeInfo(Vector2 _pointA, Vector2 _pointB, float _angleA, float _angleB)
        {
            pointA = _pointA;
            pointB = _pointB;
            angleA = _angleA;
            angleB = _angleB;
        }
    }

}
