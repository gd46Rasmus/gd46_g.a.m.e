﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public string levelName;
    public GameObject button;
	// Use this for initialization
	void Start () {
        button.SetActive(true);
    }

    public void StartGame()
    {
        for(int i = 0; i < GameMode.playerKills.Length ;i++)
        {
            GameMode.playerKills[i] = 0;
            GameMode.playerDeaths[i] = 0;
        }

        //remove button, remains if not
        button.SetActive(false);

        SceneManager.LoadScene(levelName);
    }
}
