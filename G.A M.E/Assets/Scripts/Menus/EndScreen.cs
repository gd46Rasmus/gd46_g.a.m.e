﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndScreen : MonoBehaviour {

    public Text p1Kills;
    public Text p2Kills;
    public Text p3Kills;
    public Text p4Kills;

    public Text p1Deaths;
    public Text p2Deaths;
    public Text p3Deaths;
    public Text p4Deaths;


    
    void Start ()
    {
        p1Kills.text = GameMode.playerKills[0].ToString();
        p2Kills.text = GameMode.playerKills[1].ToString();
        p3Kills.text = GameMode.playerKills[2].ToString();
        p4Kills.text = GameMode.playerKills[3].ToString();

        p1Deaths.text = GameMode.playerDeaths[0].ToString();
        p2Deaths.text = GameMode.playerDeaths[1].ToString();
        p3Deaths.text = GameMode.playerDeaths[2].ToString();
        p4Deaths.text = GameMode.playerDeaths[3].ToString();
    }
	
	public void ToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
