﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public Image meleeIcon;
    public Image rangerIcon;
    public Image assassinIcon;

    public Text stats;
    public Text time;
    private UIState currentState = UIState.Select;

	// Use this for initialization
	void Start () {
        UpdateUI();
	}
	
	// Update is called once per frame
	void Update () {
        time.text = Mathf.FloorToInt(GameMode.GetTimeLeft() / 60) + "." + Mathf.FloorToInt(GameMode.GetTimeLeft() % 60);
        stats.text = "Player 1 Kills: " + GameMode.playerKills[0] + "\nPlayer 2 Kills: " + GameMode.playerKills[1] + "\nPlayer 3 Kills: " + GameMode.playerKills[2]+ "\nPlayer 4 Kills: " + GameMode.playerKills[3];
        //p2Death.text = "Player 2 K/D " + GameMode.playerKills[1] + "/" + GameMode.playerDeaths[1];

        //p3Death.text = "Player 3 K/D " + GameMode.playerKills[2] + "/" + GameMode.playerDeaths[2];
    }

    public void UpdateUI()
    {
        //non frame to frame updates of ui
        switch(currentState)
        {
            case UIState.Select:

                meleeIcon.color = new Color(1, 1, 1, 1);
                rangerIcon.color = new Color(1, 1, 1, 1);
                assassinIcon.color = new Color(1, 1, 1, 1);

                break;
            case UIState.InGame:

                meleeIcon.color = new Color(1, 1, 1, 0);
                rangerIcon.color = new Color(1, 1, 1, 0);
                assassinIcon.color = new Color(1, 1, 1, 0);

                break;
            default:
                break;
        }
    }

    public void SetPlayer(int playerIndex, Camera playerCam)
    {
        //set layer for ui, define what player sees it
        gameObject.layer = 1;
        GetComponentInParent<Canvas>().worldCamera = playerCam;
    }

    public void SetState(UIState newState)
    {
        currentState = newState;
    }


    public enum UIState { Select, InGame}
}
