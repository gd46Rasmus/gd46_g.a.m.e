﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public class PlayerController : MonoBehaviour {

    public List<GameObject> CharactersAvailable = new List<GameObject>();

    public UIManager ui;

    bool playerIndexSet = false;
    public PlayerIndex playerIndex;
    GamePadState state;
    GamePadState prevState;

    private Character[] pawnList = new Character[3];

    public GameObject playerCamera;

    public int playerN = 1;

    public float baseRespawnTime = 10;

    private Vector3 meleePos;
    private Vector3 rangerPos;
    private Vector3 assassinPos;

    public Compass compassObject;

    private Character activePawn;

    private List<Character> compassTargets = new List<Character>();

    // Use this for initialization
    void Start () {

        assassinPos = transform.position;
        meleePos = transform.position;
        rangerPos = transform.position;

        //create a camera
        playerCamera = Instantiate(playerCamera, transform.position, transform.rotation);
        playerCamera.GetComponent<CameraController>().SetPlayer(playerN, 3);
        

        //set the camera culling mask depending on the player
        switch(playerN)
        {
            case 1:
                playerCamera.GetComponent<Camera>().cullingMask = -7169;
                break;
            case 2:
                playerCamera.GetComponent<Camera>().cullingMask = -6657;
                break;
            case 3:
                playerCamera.GetComponent<Camera>().cullingMask = -5633;
                break;
            case 4:
                playerCamera.GetComponent<Camera>().cullingMask = -19969;
                break;
        }


            
        //PlayerIndex testPlayerIndex = (PlayerIndex)(playerN - 1);
            
        
        ui.SetPlayer(playerN, playerCamera.GetComponent<Camera>());
    }
    // Update is called once per frame
    void Update() {

        //refilled every frame
        compassTargets.Clear();

        //states of gamepad for controls
        prevState = state;
        state = GamePad.GetState(playerIndex);

        //check if controlling a pawn
        if (activePawn != null)
        {
            //normal inputs
            if (state.Triggers.Right > .4f)
            {
                activePawn.Attack();
            }

            if (state.ThumbSticks.Right.X != 0 || state.ThumbSticks.Right.Y != 0)
            {
                activePawn.Rotate(state.ThumbSticks.Right.X, state.ThumbSticks.Right.Y);
                
            }
            activePawn.CameraPoint(state.ThumbSticks.Right.X, state.ThumbSticks.Right.Y);
            if (Mathf.Abs(state.ThumbSticks.Left.X) > 0.15 || Mathf.Abs(state.ThumbSticks.Left.Y) >= 0.15)
            {
                if(Mathf.Abs(state.ThumbSticks.Right.X) < .15 && Mathf.Abs(state.ThumbSticks.Right.Y) < .15)
                {
                    activePawn.Rotate(state.ThumbSticks.Left.X, state.ThumbSticks.Left.Y);
                }
                
                if(state.Triggers.Left > .4 && activePawn.CanSprint())
                {
                    activePawn.Move(state.ThumbSticks.Left.X * 2f, state.ThumbSticks.Left.Y * 2f);
                    activePawn.SprintMeter();
                }
                else
                {
                    activePawn.Move(state.ThumbSticks.Left.X, state.ThumbSticks.Left.Y);
                }
                
            }

            /**
            if(state.Buttons.Y == ButtonState.Pressed && prevState.Buttons.Y == ButtonState.Released)
            {
                activePawn.SwapLightStatus();
            }

            if (state.Buttons.A == ButtonState.Pressed && prevState.Buttons.A == ButtonState.Released)
            {
                
            }

            if(state.Buttons.B == ButtonState.Pressed && prevState.Buttons.B == ButtonState.Released)
            {
                
            }
            */
            if (state.Buttons.LeftShoulder == ButtonState.Pressed && prevState.Buttons.LeftShoulder == ButtonState.Released)
            {
                activePawn.SwapLightStatus();
            }
            if (state.Buttons.RightShoulder == ButtonState.Pressed && prevState.Buttons.RightShoulder == ButtonState.Released)
            {
                activePawn.SpecialMove();
                //activePawn.Pulse();
            }


        }
        else
        {
            //respawing
            //to be function based

            ui.SetState(UIManager.UIState.Select);
            ui.UpdateUI();
            if (state.Buttons.X == ButtonState.Pressed)
            {
                ui.SetState(UIManager.UIState.InGame);
                activePawn = Instantiate(CharactersAvailable[0], meleePos, new Quaternion()).GetComponent<Character>();
                spawn();
                ui.UpdateUI();
            }
            else if(state.Buttons.Y == ButtonState.Pressed)
            {
                ui.SetState(UIManager.UIState.InGame);
                activePawn = Instantiate(CharactersAvailable[1], rangerPos, new Quaternion()).GetComponent<Character>();
                spawn();
                ui.UpdateUI();
            }
            else if(state.Buttons.B == ButtonState.Pressed)
            {
                ui.SetState(UIManager.UIState.InGame);
                activePawn = Instantiate(CharactersAvailable[2], assassinPos, new Quaternion()).GetComponent<Character>();
                spawn();
                ui.UpdateUI();
            }
            else if(state.Buttons.LeftShoulder == ButtonState.Pressed && state.Buttons.RightShoulder == ButtonState.Pressed && state.Triggers.Left > .5f)
            {
                ui.SetState(UIManager.UIState.InGame);
                activePawn = Instantiate(CharactersAvailable[3], assassinPos, new Quaternion()).GetComponent<Character>();
                spawn();
                ui.UpdateUI();
            }
        }
    }

    void LateUpdate()
    {

        if (activePawn != null)
        {
            Compass[] compasses = activePawn.GetComponentsInChildren<Compass>();
            int diff = compasses.Length - Campfire.charsInRange.Count;
            if (diff > 0)
            {
                for (int i = 0; i < diff; i++)
                {
                    Destroy(compasses[compasses.Length - i - 1].gameObject);
                }

                //delete compasses not in use
            }
            else if (diff < 0)
            {
                //create new compasses where they are missing
                for (int i = 0; i < -diff; i++)
                {

                    Compass spawned = Instantiate(compassObject, transform);
                    spawned.gameObject.transform.SetParent(activePawn.gameObject.transform);
                    spawned.transform.localScale = new Vector3(7, 7, 7);
                    spawned.SetLayer(activePawn.area.gameObject.layer);
                    spawned.transform.localPosition = new Vector3(0, 0, 0);
                }
                compasses = activePawn.GetComponentsInChildren<Compass>();
            }

            for (int i = 0; i < Campfire.charsInRange.Count; i++)
            {
                compasses[i].SetRotation(Campfire.charsInRange[i]);
            }
        }

        //compassTargets.Clear();
    }

    private void spawn()
    {
        activePawn.SetPlayerLayer(playerN);
        activePawn.SetPlayerController(this);
        playerCamera.GetComponent<CameraController>().NewTarget(activePawn);
    }

    public void CompassTarget(Character newChar)
    {
        compassTargets.Add(newChar);
    }

    public PlayerIndex getPlayerIndex()
    {
        return playerIndex;
    }
}