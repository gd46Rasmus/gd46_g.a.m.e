﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public class Sword : MonoBehaviour {

    public float damage = 200;
    //deal damage if hit a character
    void OnTriggerEnter2D(Collider2D otherPawn)
    {

        Character otherChar = otherPawn.GetComponentInParent<Character>();

        if (otherChar != null)
        {
            Character owner = GetComponentInParent<Character>();

            GamePad.SetVibration(owner.GetPlayerController().getPlayerIndex(), 1, 1);
            owner.StartCoroutine(owner.doHaptic(.3f));

            otherChar.TakeDamage(damage, GetComponentInParent<Character>());
        }
    }
}
