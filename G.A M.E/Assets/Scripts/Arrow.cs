﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : Projectile {

    public Vector3 targetScale;
    //to not make arrow disapear from scaling down
    public float shrinkBuf = .5f;

    void Start(){}

    public void Spawn()
    {
        //give a speed in forward direction
        GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Cos(transform.rotation.eulerAngles.z * Mathf.Deg2Rad) * speed, Mathf.Sin(transform.rotation.eulerAngles.z * Mathf.Deg2Rad) * speed);
        //kill when its used its life time
        StartCoroutine(Die());

        //shrink as it moves away
        Tween shrink = transform.DOScale(targetScale, lifeSpan + shrinkBuf);
        shrink.SetEase(Ease.InExpo);
    }
}
